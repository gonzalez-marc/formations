# Les spécifications fonctionnelles

## Table des matières
1. [Un peu de communication](#communcation)
2. [User stories et agilité](#stories)
    1. [Quelques critères](#criteres)
    2. [La valeur ajoutée](#valeur-ajoutee)
    3. [Qui doit écrire les user stories ?](#who)
    4. [Qu'est-ce qui fait une bonne use story ?](#quality)
    5. [Les critères d'acceptation](#acceptance)
3. [Rédiger des users stories](#write)
    1. [Quelques propriétés que devraient posséder les user stories](#properties)
    2. [Comment détailler une user story ? Quand ?](#how)
    3. [Comment découper une user story ?](#cut)
4. [Les patterns de découpe](#patterns)
    1. [Quelques anti-patterns de découpage](#anti-cut)
    2. [Les anti-patterns des stories](#anti-cut)
    3. [Les personas](#personas)
    4. [Comment regrouper les stories ?](#regroup)
5. [Références](#referencies)

## 1) Un peu de communication<a name="communcation"></a>

La communication entre ceux qui veulent et ceux qui construisent un logiciel  peut se schématiser de la manière suivante :


```
Ceux qui veulent                                                           Ceux qui construisent le logiciel
----------------                                                           ---------------------------------
- Les clients                                                               - les développeurs
- les utilisateurs
- les analystes                            <-- Les spécifications  
- les experts métiers                            fonctionnelles -->
- les "non-développeurs" du métier 
  ou d'une organisation donnée
```

Il y a un besoin d'équilibre entre les envies des stakeholders et la capacité de production des développeurs : si les fonctionnalités envisagées sont trop vastes ou mal définies, les développeurs ne comprennent pas ce qui est réellement attendu, livrable. La qualité s'en retrouve négligée au profit de deadlines parfois irréalistes. Certaines fonctionnalités peuvent se retrouver partiellement implémentées, les développeurs prennant des décisions de leur propore chef alors qu'ils devraient impliquer les parties prenantes concernées.


## 2) User stories et agilité<a name="stories"></a>

#### 2.1 Quelques critères<a name="criteres"></a>

Les user stories sont au centre du processus du agile permettant de construire un logiciel et doivent répondre aux critères suivants :
- le logiciel qui résulte de l'écriture des users stories doit être **fonctionnel**
- Les user stories doivent satisfaire le besoin d'un utilisateur en permettant la livraison rapide et continue de fonctionnalités ayant de la valeur ajoutée
- Elles doivent rester simples, compréhensibles
- L'incrément logiciel qui résulte d'une user story doit donner à l'utilisateur la possibilité d'effectuer une action qui a de la valeur ajoutée. Le suivi du travail lié aux users stories doit permettre d'évaluer la valeur ajoutée d'une nouvelle fonctionnalité


### 2.2 La valeur ajoutée <a name="valeur-ajoutee"></a>

Cette notion de valeur ajoutée est au centre du processus agile, elle doit notamment permettre la priorisation des tâches à effectuer, afin de livrer régulièrement ; en limitant les risques liés au projet.

Il faut bien comprendre que lors du  développement d'un logiciel, de nouvelles idées émergent. Au fur et à mesure des versions les parties prenantes changent d'avis, ce qui est tout à fait **normal**. Il convient donc d'écrire régulièrement des user stories, à tout moment du projet, en gardant à l'esprit que c'est un bon moyen d'obtenir des informations sur des pans métier d'un projet, tôt et régulièrement.

Il faut également garder à l'esprit qu'il est impossible de prédire l'évolution d'un logiciel. De ce fait les développeurs sont **incapables** d'évaluer avec précision le temps de développement d'une fonctionnalité.

Enfin, il ne faut paa oublier que si les logiciels échoutent, c'est bien souvent parce que les spécfications ne s'attachent pas à porter cette notion de valeur ajoutée qui pourtant est au centre du besoin ; qui donne du sens à un logiciel. 


### 2.3 Qui doit écrire les user stories ?<a name="who"></a>

Dans l'idéal c'est au métier  avec l'aide du product owner d'écrire les user-stories, de manière collégiale, afin d'apporter plus d'informations possibles, en pansant "user first".

Toutefois, les développeurs doivent prendre partie à la rédaction de ces stories, notamment quand il s'agit d'affiner le découpage parce qu'elles peuvent parfois nécessiter une bonne connaissance technique.

L'utilisateur final, le PO, doivent prendre le lead sur la priorisation des user stories. Là encore, l'aide des développeurs peut être nécessaire afin de de déterminer ce qu'il est possible de faire, et dans quel ordre. L'objecfif est ici de rendre ces stories les plus indépendantes les unes des autres


### 2.4 Qu'est-ce qui fait une bonne user story ?<a name="quality"></a>

**Il ne faut pas négliger** la rédaction d'user stories. Mieux elles sont rédigées, plus la qualité du développement s'en retrouve améliorée.
Elles représentent la base sur laquelle beaucoup de temps va être consacré à la planification et au dévelopement ; il faut donc les écrire correctement.

1. Qui est la persona ? (**En tant que...**)
2. Que veut l'utilisateur ? (**j'ai besoin de/souhaite/m'attends à...**)
L'objectif est l'élément le plus important d'une user story parce qu'il précise comment on répond à une problématique, quand est-ce que l'on décide qu'une user story est terminée (avec l'aide de critères d'acceptation)
3. Pourquoi ? (**Afin de...**)
Certaines personnes considèrent parfois cette partie optionnelle mais elle donne du sens à la compréhension de la problématique que l'on souhaite résoudre
4. Quand est-ce qu'une story est-elle terminée ? (**C'est terminé quand...**). ON définit ici une liste de critères qui définissent une story comme terminée. On peut également y adjoindre toute information, lien, permettant de mettre en évidence les critères d'acceptation


**Exemple : Les utilisateurs d'un site web**

Par exemple, lorsque nous créons un site Web qui a deux types d'utilisateurs - les utilisateurs connectés et les invités - nous sommes susceptibles d'écrire les critères d'acceptation suivants pour une User Story qui définit la fonctionnalité de connexion pour un utilisateur déconnecté :

<u>Story</u>
> **En tant qu'** utilisateur déconnecté<br/>
**je souhaite** pouvoir me connecter au site web<br/>
**afin de** pouvoir accéder à mon espace personnel<br/>

<u>Critère d'acceptation</u> ([langage Gherkin](https://cucumber.io/docs/gherkin/reference/)) :
> **Scenario** : L'utilisateur système se connecter avec des informations d'identification valides<br/>
**Étant donné** que je suis un utilisateur système déconnecté et que je suis sur la page de connexion,<br/>
**Lorsque** je remplis les champs «Nom d'utilisateur» et «Mot de passe» avec mes informations d'authentification et que je clique sur le bouton Connexion,<br/>
**Alors** le système me connecte


**Exemple : recherche d’un ouvrage par un abonné d’une médiathèque**

Deuxième exemple, la recherche d’un ouvrage sur le site d’une médiathèque. L’US, dont le titre est « Consulter la fiche d’un ouvrage » se décrit comme :


<u>Story</u>
> **En tant qu’** abonné de la médiathèque<br/>
**Je recherche** un ouvrage en spécifiant son auteur, son titre ou sa référence<br/>
**Afin que** l’emprunter ou de le réserver

<u>Critère d'acceptation</u> (sous forme de liste) :
> Résultat unique, afficher l’ouvrage : auteur/titre/reference<br/>
Pas de résultat, inviter l’abonné à une nouvelle recherche<br/>
Plusieurs résultats, afficher la liste des ouvrages triés par auteur


<u>Critère d'acceptation</u> ([langage Gherkin](https://cucumber.io/docs/gherkin/reference/)) :
>  **Scenario** : l’abonné lance une recherche pour consulter la fiche d’un ouvrage<br/>
**Étant donné** que je suis abonné et que je suis sur la page de recherche d’un ouvrage<br/>
**Lorsque** je saisis l’auteur, le titre ou la référence et que je lance la recherche<br/>
**Alors** je peux consulter la liste des ouvrages correspondants triés par auteur

### 2.5 Les critères d'acceptation <a name="acceptance"></a>
Les critères d’acceptation sont les conditions de satisfaction d’un récit utilisateur :

- Ils ont pour objectif de clarifier pour l’équipe de réalisation les attentes et exigences du métier.
- Ils fixent les conditions qui permettent de satisfaire l’utilisateur.
- Le Product Owner est garant des critères d’acceptation et de leur compréhension par l’équipe de réalisation, au même titre que la description du récit.
- Le Product Owner accepte l’implémentation du récit utilisateur en fin de Sprint sur la base des critères d’acceptation.
- Respecter seulement les critères d’acceptation ne signifie pas forcément la validation totale du récit. La validation définitive sera assurée par les tests fonctionnels.


Pour conclure, les critères d’acceptation sont indispensables car ils permettent de mettre tout le monde d’accord sur la manière dont va être vérifiée l’implémentation d’une US.

## 3) Rédiger des users stories

### 3.1 Quelques propriétés que devraient posséder les user stories <a name="properties"></a>

1. **Indépendance** :<br/>
Dans la mesure du possible les user stories doivent être indépendantes les unes des autres, ce qui signifie qu'elles doivent pouvoir être développées dans n'importe que l'ordre.<br/>
La dépendance entre les user stories entrave la communication entre les développeurs et complique la priorisation ainsi que lla planification. 

2. **Négociable**<br/> 
Une user story est une promesse de d'échange, de dialogue. Elle devrait avoir ce qu'il faut comme détails et ceux-ci devraient être ajoutés le plus tard possible

3. **Évalulable**

4. **Contenue**<br/>
La taille des stories doit être limitée, suffisamment petite pour ne pas dépasser un certain nombre de points de complexité. L'échelle est à définir avec l'équipe. On peut par exemple considérer que dès lors qu'une tâche dépasse 8 points de complexité, il convient alors de la redécouper

5. **Testable<br/>
Les critères d'acceptance doivent être mis en avant afin que les développeurs puissent savoir quand est-ce qu'ils ont terminé


### 3.2 Comment détailler une story ? Quand ? <a name="how"></a>

Il faut garder à l'esprit qu'une story n'est pas une spécification complète. Elle est destinée à emmener une discussion ultérieure au plus près du moment de l'implémentation. Pour autant cela n'enlève à rien les discussions entre l'équipe de développement et les stakeholders qui  doivent être continus, tout au long du projet ; pas seulement au début. 

De ce fait, l'ajout des détails à une story doit se faire **à partir du moment où on a une bonne compréhension du sujet**. Il faut accepter qu'il est impossible de tout connaître à l'avance. Il n'est pas non plus nécessaire de réfléchir à une fonctionnalité tant que l'on est pas vraiment certain qu'elle est nécessaire.

Dans cette logique de discussions, et tant que l'implémentation n'est pas terminée on peut ut fsiliser la carte de la story comme un espace d'échange. Il convient donc d'y noter les points importants, afin de pouvoir reprendre la discussion plus tard même si les personnes qui ont eu la discussion sont différentes. 

En résumé il faut :
- Découper les stories en la plus petite unité possible qui pourra être développée par un utilisateur. Plus la story est précise, plus il sera facile de l'évaluer
- Ajouter les critères d'acceptation, les notes, liens et autres ressources qui permettent la compréhension du besoin
- Ajouter des alertes pour discuter des éléments sur lesquels discuter dans le cadre de l'implémentation
- Limiter les informations au strict nécessaire
- Séparer sous la forme de sections les notes, tests, critères qui caractérisent la story
- Pour ce qui est des tests, on peut préciser les points à évaluer comme par exemple : *tester le système de paiement avec une VISA, tester le système de paiement avec une Mastercard, tester avec une carte expirée*, etc.

### 3.3 Comment découper une user story ? <a name="cut"></a>

Il est important de découper les stories parce que les développeurs ont besoin de pouvoir isoler la complexité, parce qu'il est nécessaire de réduire la complexité cognitive des tâches à effectuer. Dans ce contexte le PO n'est pas seul et doit se faire aider des principaux concernés.

Toutes les stories n'ont pas la même priorité, c'est pourquoi il est important de les découper. Cela permet notamment de déprioriser certains aspects métiers, l'essentiel de la complexité venant principalement d'une petite partie du code (principe de Pareto, 20/80).

L'exemple suivant...

> En tant qu'utilisateur, je veux sauvegarder l'essentiel de mon disque

...peut se découper de manière suivante :

> En tant qu'utilisateur avancé, je veux spécifier les fichiers et dossier à sauvegarder en me basant sur leur taille, leur date de création et de modification<br/>
> En tant qu'utilisateur, je veux indiquer queles sont les dossiers à ne pas sauvegarder afin de ne pas sauvegarder ce qui ne nécessite pas de l'être 

L'exemple suivant...
> En tant qu'utilisateur je souhaite me connecter à l'application en utilisant un de mes comptes de réseaux sociaux

... peut se découper ainsi :
> En tant qu'utilisateur, je souhaite pouvoir me connecter via mon compte Facebook<br/>
> En tant qu'utilisateur, je souhaite pouvoir me connecter via mon compte LinkedIn<br/>
> En tant qu'utilisateur, je souhaite pouvoir me connecter via mon compte Twitter<br/>

## 4) Les patterns de découpe <a name="patterns"></a>
[Agile For All](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/) propose différents patterns pour découper une user story

1. Workflow

Écrire le cas d'usage nominal ou le plus simple en premier permet d'affronter la complexité et ainsi déterminer quels sont les autres uses cases
>En tant que gestionnaire de contenu, je veux pouvoir publier des articles sur le site corporate de la société

On peut imaginer que les nouvelles stories qui en découlent soient les suivantes :
>...je peux publier une actualité après une révision par le responsable marketing<br/>
...je peux publier une actualité sur un site de staging<br/>
...je peux publier une actualité depuis un site de staging vers le site en production

2. Variation des règles métier

>En tant qu'utilisateur, je souhaite rechercher des dates de vols flexibles

Les stories qui en découlent peuvent être les suivantes :
>...je souhaite pouvoir rechercher des dates de vols de N jours entre X et Y<br/>
...je souhaite pouvoir rechercher des dates de vols sur un week end

3. L'effort majeur

> En tant qu'utilisateur, je veux payer mon vol avec une carte VISA, MasterCard ou American Express

Les stories qui en découlent peuvent être les suivantes :
> ...je peux payer mon vol avec une carte VISA<br/>
...je peux payer mon vol avec une carte MasterCard<br/>
...je peux payer mon vol avec une carte American Express

L'essentiel de l'effort srea mis sur la toute première storie. En outre il se dégage une dépendance entre les stories, la technique étant principalement implémentée dans la première.<br/>
Ce type de découpage a l'avantage de permettre de modifier l'ordre dans lequel seront implémentées les stories, notamment en fonction de la complexité technique. Par exemple VISA sera peut être plus facile à implémenter que American Express. 

4. Simple / Complexe

L'idée est de définir la version la plus simple d'un use case comme une user story. Chaque story qui est ensuite écrite découle de la précédente et représente une version plus complexe de celle-ci. Les critères d'acceptation permettent ici de cadrer l'écriture. 
> En tant qu'utilisateur je souhaite comparer les vols de 2 destinations<br/>
...en spécificiant un nombre maximum de stops<br/>
...en incluant les aéroports proches<br/>
...en incluant de la flexibilité sur les dates

5. Variation des détails

Séparer les stories suivant les variations de données peut être un bon moyen de découpe.
> En tant qu'utilisateur je souhaite rechercher des transporteurs livrant entre un point A et un point B<br/>
...filtrés par région<br/>
...filtrés par région et par département

> En tant qu'utilisateur je souhaite rechercher des accessoires pour mon appareil photo afin de ne pas avoir à parcourir tout le catalogue<br/>
En tant qu'utilisateur je souhaite recherchercher des produits par leur nom et leur description afin de ne pas avoir à parcourir tout le catalogue<br/>
En tant qu'utilisateur je souhaite recherchercher des produits par leur prix et leur disponibilité afin de ne sélectionner que les produits que je suis susceptible d'acheter<br/>

6. Qualité vs Utilité vs Usabilité

Dans le dcadre de d'interfaces complexes il peut être utile de découper les stories en partant de l'élément d'UX le plus simple à implémenter

> En tant qu'utilisateur je souhaite rechercher des vols entre 2 desinations<br/>
...en utilisant un simple champ date<br/>
... en utilisant un calendrie

7. Différer la performance

Quand une user story est impactée par la performance mais qu'elle a toujours une valeur pour l'utilisateur il convient de caractériser cette performance.
> En tant qu'utilisateur, je souhaite rechercher les vols disponibles entre 2 destinations<br/>
...en moins de 5 secondes<br/>
...lentement, en affichant un loader

8. Crud

Le mot "manager" induit plus actions. On peut donc découper la story en conséquence
> En tant qu'utilisateur je peux me connecter à mon espace utilisateur<br/>
...je peux éditer mes informations personnelles
...je peux supprimer mon compte

9. Spike limité dans le temps

Il arrive parfois que l'on ne soit pas en mesure d'évaluer une ou plusieurs stories, et ce, parce que l'on manque de connaissances techniques ou encore métier. On a donc besoin de réduire le risque et l'incertitude et ainsi découvrir de nouveaux moyens de découper une story.

10. Découpage par persona

Un autre moyen de découper une story est de différencier par persona

> En tant qu'utilisateur non connecté je souhaite visualiser la liste des produits<br/
En tant qu'utilisateur coWhen a large part of the effort is making it fast. But you can still learn from the slow version, and it still has some value to the user who can now undertake the action albeit slowly.nnecté je souhaite visualiser la liste des produits personnalisée grâce à mes préférences

### 4.1 Quelques anti-patterns de découpage <a name="anti-patterns"></a>
- 99% du travail à réaliser se retrouve dans une seule story
- Découper une storie selon les principes du backend / frontend n'apporte aucune valeur ajoutée. Le métier doit s'aborder de manière verticale (UI / Business / Persistance) et la story doit donc prendre en compte l'intégralité de la stack.

### 4.2 Les anti-patterns des stories <a name="anti-patterns-stories"></a>
> En tant que développeur je souhaite une base de données afin de stocker les informations que l'application nécessite

**C'est un prerequis pour les autres stories**. Toute l'application nécessite que cette tâche soit implémentée en premier

**Elle dépend des autres stories**. Il est impossible de savoir comment modéliser la base de données, sans s'être penché sur les autres briques du système

**Elle n'a aucune valeur ajoutée pour l'utilisateur**. Il faut garder à l'esprit que l'utilisateur doit bénéficier d'une nouvelle fonctionnalité ou d'une amélioration de l'existant pour y trouver un intérêt.

1. La circularité
Considérons l'user story suivante :
> **En tant qu'** *utilisateur*<br/>
**je souhaite** changer mon mot de passe<br/>
**afin d'**obtenir un nouveau mot de passe

Ici *je souhaite* et *afin de* ont la même signification. On pourrait très bien supprimer le bénéfice exprimé mais on perd cette notion de valeur ajoutée pour l'utilisateur.

On pourrait plutôt écrire :
> **En tant qu'** *utilisateur*<br/>
**je souhaite** changer mon mot de passe<br/>
**afin de ** renforcer la sécurité de mon compte et être conforme à la nouvelle politique de renouvellement des mots de passe de la société

2. Décrire une solution plutôt qu'un problème

Il est nécesaire de garder à l'esprit que l'objectif d'une story est de décrire un cas d'usage fonctionnel et non de proposer une solution technique. Caractériser systématiquement le bénéfice pour l'utilisateur permet d'éviter ce piège 

3. Mauvaise persona:

Il est indispensable de bien identifier la partie prenante décrite dans la story

4. Trop de détails

Il n'est pas nécessaire de connaître tous les détails métier avant de commencer la rédaction d'une story. C'est un espace de discussion, d'échange dont les détails doivent être précisés au plus près du moment de l'implémentation.

5. Pas assez de détails

Moins la story est précise, plus il est difficile de répondre réellement au besoin métier

6. Dépendances entre les stories 

Les stories doivent pouvoir être parallélisées, faiblement couplées.

7. Les users stories ne sont pas du waterfall

Il ne s'agit pas d'un document de spécifications ! Une story concerne un besoin bien précis, implémenté par une personne bien précise (graphiste, développeur, etc). Toutes les stories doivent pouvoir être parallélisées et donc, se terminer au même moment.

8. Tout n'a pas besoin d'être une user story

Pour les tâches purement techniques ou éloignées du cadre de l'utilisateur il est possible d'écrire les spécifications en faisant du FDD (Feature Driven Development)

```
[action (verbe)] le [élément] [par|pour|de|à] [objet]
```

Exemples : 
- Générer un identifiant pour une transaction
- Changer le texte d'un label

### 4.3 Les personas <a name="personas"></a>
Il faut caractériser les personas et ne pas se contenter d'utiliser systématiquement "utilisateur" qui pourra par exemple être remplacé par :
1. utilisateur non connecté
2. utilisateur connecté
3. utilisateur premium
4. utilisateur invité
5. utilisateur effectuant sa première visite
6. utilisateur fraîchement enregsitré

Dans les 4 premiers cas on parlera d'utilisateur de premier ordre, tandis que les cas 5 et 6 sont des utilisateurs "décorés". Ils sont eux aussi importants mais ne sont pas vraiment des types de persona à part entière

### 4.4 Comment regrouper les stories ? <a name="regroup"></a>
Il n'est pas nécessaire de complexifier l'organisation des stories, la méthodologie Scrum propose déjà un découpage out of the box.

<u>Thème</u>: 
Il s'agit d'un groupement de stories ou de features qui peuvent caractériser par exemple un parcours psécifique, un utilisateur précis. Dans le cas où on regroupe en features, les stories viendront caractériser, composer, chacune d'entre elles

<u>Épique</u> :
Une épique est juste une "grosse" story, qui est trop grosse pour être implémentée.

<u>Tâche</u> :
Afin de pouvoir implémenter une story il peut être nécessaire au développeur, au graphiste, de redécouper celle-ci en tâches afin d'organiser au mieux son travail. Elle doit être limité à un type bien précis d'action : test unitaire, UI, analyse, etc


## 5) Références <a name="referencies"></a>
- [https://blogs.harvard.edu/markshead/good-user-stories/](https://blogs.harvard.edu/markshead/good-user-stories/)
- [https://www.gov.uk/service-manual/agile-delivery/writing-user-stories](https://www.gov.uk/service-manual/agile-delivery/writing-user-stories)
- [https://www.mountaingoatsoftware.com/blog/tag/user-stories](https://www.mountaingoatsoftware.com/blog/tag/user-stories)
- [https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/)
- [https://blog.jbrains.ca/permalink/three-steps-to-a-useful-minimal-feature](https://blog.jbrains.ca/permalink/three-steps-to-a-useful-minimal-feature)
- [https://en.wikipedia.org/wiki/User_story#Examples](https://enhttps://www.mountaingoatsoftware.com/blog/the-two-ways-to-add-detail-to-user-stories.wikipedia.org/wiki/User_story#Examples)
- [https://www.mountaingoatsoftware.com/blog/the-two-ways-to-add-detail-to-user-stories](https://www.mountaingoatsoftware.com/blog/the-two-ways-to-add-detail-to-user-stories)

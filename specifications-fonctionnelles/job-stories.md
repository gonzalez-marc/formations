# Attendu
Quel est le problème à résoudre ?
- Quand [situation], en tant que [utilisateur], je veux [motivation] afin de [résultat souhaité]

# Pourquoi c'est important ?
Pourquoi est-ce qu'on doit résoudre le problème ? Qui va être impacté ?
- Raison 1
- Raison 2
- ...

# Contraintes à prendre en compte
Quelles sont les contraintes qu'il ne faut pas négliger ?
- Ex : environnement technique
- Ex : contrainte de calendrier
- ...


# Historique des échanges
Historiser les échanges et surtout à quoi ils ont abouti et pourquoi (cf légende).

# Benchmark / Inspiration
Que font les autres ? Comment cela peut-il nous inspirer graphiquement ou fonctionnellement ?
- Liend dribbble

# Livrables 
- Wireframes et workflow
Quelle est la solution ? Comment intéragit-elle avec l'existant ? 
Ex : lien invision, draw, etc

- UI / UX
La solution trop stylée au problème : 
Ex : lien figma, xd, etc

# Specs devs
Lister ici tous les éléments qui ne sont pas présents dans lesq maquettes mais qui doivent être pris en compte lors de la phase de développement.
Ex : labels des éléments cachés, comportement en responsive, etc

# Tests d'acceptation
Lister les scénarios permettant de déterminer que le métier est couvert (Syntaxe Gherkin)

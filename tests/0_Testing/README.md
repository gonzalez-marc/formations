# Table des matières
1. [De quoi parle-t-on ?](#what_is_testing)
2. [Les différents types de tests](#different_test_scopes)
3. [Les deux approches](#the_two_ways)
4. [Différents types de tests](#different_test_types)
5. [Un point sur les doublures](#tests_and_doubles)
6. [Quand écrire un test: le TDD](#when_write_tdd)
7. [Le kata Bowling pour s'initier au TDD](#bowling_kata_tdd)
8. [Pour aller plus loin: une réflexion sur quoi tester](#tests_questioning)


# 1. De quoi parle-t-on ? <a name="what_is_testing"></a>
<div style="text-align: justify">

Le test de logiciel est une méthode pour vérifier si le produit logiciel réel correspond aux exigences attendues en terme de fonctionnalités, de sécurité, et de qualité; et pour s'assurer que le produit logiciel est exempt de défauts. Cela implique l'exécution de composants logiciels/système à l'aide d'outils manuels ou automatisés pour évaluer une ou plusieurs propriétés d'intérêt. Le but des tests logiciels est d'identifier les erreurs, les lacunes ou les exigences manquantes par rapport aux exigences réelles.

Certains préfèrent définir les tests logiciels comme un test de boîte blanche et de boîte noire. En termes simples, les tests de logiciels signifient la vérification de l'application sous test (Verification of Application Under Test - AUT). Cette formation sur les tests de logiciels a pour objectif de justifier l'importance des tests de logiciels, tout en présentant certains outils et méthodes utilisés.
</div>

# 2. Les différentes portées des tests <a name="different_test_scopes"></a>
<div style="text-align: justify">

Pour chaque projet applicatif, plusieurs éléments sont à tester, et sous différents angles et approches. Pour répondre à ce défi, les équipes peuvent mettre en place des tests selon différentes portées, telles que présentées sous forme de pyramide par Mike Cohn comme suit:

<div align="center">

![small_tests_pyramid.png](./images/model_tests_pyramid_small.png)

</div>

|TYPE DE TEST|DESCRIPTION|
|:---|:---|
|Unitaire|Suite d'opérations qui vise à valider un comportement unique (méthode <br>ou sous ensemble d’une méthode) issu d’un cas d’utilisation métier en <br>isolation du reste du monde, et donc indépendamment les uns des autres.|
|Service|Suite d’opérations permettant de vérifier la validité d’unités individuelles, <br>qui, regroupées, doivent fonctionner ensemble correctement.|
|API / IHM|Suite d'opérations visant à intégrer les éléments consommateurs des services.|

À la base de la pyramide des tests se trouvent les tests unitaires. Les tests unitaires doivent être le socle d'une solide stratégie d'automatisation des tests et, en tant que tels, représentent la plus grande partie de la pyramide.
Ils sont nombreux car légers, spécifiques, indépendants, rapides, et donc peu coûteux.
**Remarque**: l'acronyme FIRST est un bon moyen de se rappeler de ce que doit être un test unitaire:
- Fast : il doit être rapide, on peut le lancer à tout moment sans que ça prenne du temps. Ça doit être dans l'ordre des secondes et non pas des minutes.
- Isolated / Independent : il ne faut pas qu'il y ait un ordre d'exécution des tests. Un test doit être indépendant de tous les tests qui l'entourent. Et une seule cause doit être à l'origine de son échec et non plusieurs.
- Repeatable : un test doit être déterministe. Si je le lance plusieurs fois avec une même donnée en entrée le résultat ne doit jamais changer.
- Self validating : le test doit faire la validation lui-même. Pas besoin donc d'intervention manuelle pour savoir s'il est passé ou a échoué.
- Timely / thorough : le test doit être minutieux car il doit être précis par rapport à ce qu'il teste.

Les tests de service concernent le comportement attendu de l'application sur ses fonctionnalités métiers. On cherchera ici à vérifier que l'ensemble des composants mis en oeuvre découle sur le résultat attendu. Plus coûteux que les tests unitaires, on les retrouve en moins grande quantité, mais ils restent néanmoins un filet de sécurité fonctionnel des plus pertinents.

**Remarque**: il est intéressant de noter que les tests de service sont souvent confondus avec des tests d'API / IHM, à tord ceci dit. On usera de doublures de tests pour émuler les interactions avec l'extérieur, et ces tests peuvent aussi présenter l'avantage de tracer la documentation fonctionnelle du projet.

Enfin, les tests automatisés de l'interface utilisateur (ou plus généralement, des entrées/saisies externes) sont placés au sommet de la pyramide des tests car nous voulons en faire le moins possible. Ce sont des tests lourds en ressources et en performances, représentant donc un investissement non négligeable.
</div>

# 3. Les deux approches <a name="the_two_ways"></a>
<div style="text-align: justify">

Idéalement, une stratégie d'automatisation de tests devraient suivre ce modèle pyramidal présenté par Mike Cohn. Dans cette pyramide des tests, on constate que le coût des tests augmente en grimpant les échellons, en corrélation de la réduction du nombre de tests.

Cette pyramide représente également la précision des tests, en lien direct avec la portée et le nombre des tests. Un test unitaire saura indiquer quel fichier de code et quelle ligne sont concernés par une erreur, cela nécessitera peut-être d'examiner le code environnant, mais un test IHM indiquera simplement que l'application a rencontré un problème, sans forcément plus de précision.

Malheureusement, on retrouve plus couramment le modèle suivant dans les entreprises:

<div align="center">

![tests_icecream.png](./images/model_tests_icecream.png)
<br>Source : ScrumLife

</div>

Modèle dans lequel les tests unitaires sont très peu nombreux, et dans lequel, au contraire, les principaux tests réalisés sont des validations manuelles.
Si ces validations manuelles et ces tests exploratoires sont importants et nécessaires pour ressentir et interpréter l'expérience utilisateur, ils nécessitent cependant un énorme investissement temporel, et ne peuvent garantir certains aspects de la sécurité des tests, la non-régression par exemple, ou encore la validité de certains cas très particuliers et à la marge.
</div>

# 4. Différents types de tests <a name="different_test_types"></a>
<div style="text-align: justify">

Pour n'en présenter que quelques uns, on peut distinguer:
- Les Tests Unitaires, qui visent à tester chaque unité de code indépendamment, en définissant des données initiales et un résultat attendu, en récupérant le résultat de l'exécution du code, et en comparant l'attendu avec le résultat produit.
- Les Tests mis en place dans l'approche BDD (Behaviour Driven Development, voir la formation dédiée sur ce sujet) qui ont pour objectif de documenter le comportement des fonctionnalités métiers attendus sous forme de scénarios.
- Le Property-Based Testing (PBT), le Test Basé sur les Propriétés, approche de tests transverse qui peut se mettre en place aussi bien au niveau unitaire que des services, et dont l'idée est de comparer des invariants à des propriétés des résultats calculés, au travers de données initiales générées aléatoirement par le système.
- Le Fuzzing, très ressemblant au PBT, mais dont l'objectif est d'éprouver la solidité et la stabilité du projet final en cherchant, via des entrées aléatoires, à provoquer un crash.

**Remarque**: les tests PBT sont intégrés dans le code de l'application, et fonctionnent en interne, alors que les tests de Fuzz font plus appel à une plateforme externe qui va venir challenger les services exposés.

Les stratégies de tests peuvent être nombreuses et variées, la liste ci-dessus est loin d'être exhaustive, et représente une base de travail solide.

Cette dernière peut être complétée par des tests API / IHM, par des tests exploratoires / manuels, par des tests de charge et de performance, par des tests basés sur les risques, par des tests basés sur les contextes, etc etc ...

Le code source produit n'est pas le seul susceptible d'être testé, système et matériel peuvent également être intéressant à explorer, dans un second temps.

</div>

# 5. Un point sur les doublures <a name="tests_and_doubles"></a>
<div style="text-align: justify">

Lors de l'écriture des tests survient régulièrement la difficulté de devoir faire appel à des éléments extérieurs à l'application, comme une base de données ou un service web. A l'exception des tests Bout-en-Bout (End-To-End) dont le rôle est de valider l'intégralité de la chaine de communication, il est préférable d'utiliser des doublures pour simuler ces éléments extérieurs, ou s'il est nécessaire de tester des situations exceptionnelles (pour tester du code non-déterministe par exemple).

Quand on parle de test logiciel, une doublure est un objet qui va, au moment de l'exécution, remplacer un objet manipulé par du code. Le code manipulateur ne s'en rend pas compte, une bonne doublure trompe le code appelant qui doit agir comme s'il manipulait un objet normal. Écrire des doublures ne requiert aucune compétence technique particulière autre qu'écrire du code dans le langage de l'application testée ou d'apprendre à utiliser de petites bibliothèques. En revanche, la création de doublures efficaces fait appel à la ruse du développeur. Il faut maîtriser le langage et savoir jouer avec le polymorphisme et les petites subtilités de chaque langage pour réaliser les doublures les plus simples possibles.

Comme nous allons le voir, il existe plusieurs types de doublures. Pour illustrer ces différences, quelques tests seront mis en exemple dans la liste ci-dessous sur le thème: mise en place d'une messagerie utilisateur. La partie authentification de l'utilisateur est considérée comme en cours de développement par une autre équipe, et n'est donc pas disponible actuellement, le système de messagerie nécessite cependant que l'utilisateur soit authentifié, d'où l'usage des doublures. 

Les exemples ci-dessous proviennent de [wikibooks](https://fr.wikibooks.org/wiki/Introduction_au_test_logiciel/Doublures_de_test), et sont traduits du langage Java vers le langage PHP; le framework de testing utilisé est PHPUnit (Documentation disponible [ici](https://phpunit.readthedocs.io/fr/latest/)), mais les mêmes principes s'appliquent à toute technologie.

## LES BOUCHONS (STUBS)
Un bouchon est le type le plus simple de doublure, il s'agit d'une classe, écrite à la main. Son implémentation tient compte du contexte exact pour lequel on a besoin d'une doublure. Quand on écrit un bouchon on considère tout le système comme une boîte blanche, on sait comment il est implémenté on en tient compte pour écrire le bouchon le plus simple possible, avec le minimum de code nécessaire pour qu'il puisse remplir son rôle de doublure.

<div align="center">

## EXEMPLE DE BOUCHON / STUB

</div>

L'arborescence du projet ExampleTests est construit de la manière suivante depuis la racine du projet:
- **src**
    - **Identification**
        - *IdentificationUtilisateurContract.php*
    - **Messagerie**
        - *MessagerieUtilisateur.php*
    - **Exceptions**
        - *IdentificationException.php*
- **tests**
    - *MessagerieUtilisateurTests.php*
- *composer.json*

Le fichier *IdentificationUtilisateurContract.php* contient le code suivant:
``` php
<?php

namespace Maxicoffee\ExampleTests\Identification;

/**
 * permet de vérifier qu'un utilisateur a bien accès au système
 */
interface IdentificationUtilisateurContract
{
    /**
     * vérifie qu'un utilisateur existe et que son mot de passe est le bon
     *
     * @param string $identifiant
     * @param string $motDePasse
     * 
     * @return bool
     * 
     */
    public function identifier(string $identifiant, string $motDePasse): bool;  
}
```

Le fichier *MessagerieUtilisateur.php* contient le code suivant:
``` php
<?php

namespace Maxicoffee\ExampleTests\Messagerie;

use Maxicoffee\ExampleTests\AuthUser\IdentificationUtilisateurContract;

class MessagerieUtilisateur 
{
    /**
     * @param IdentificationUtilisateurContract $identification
     * 
     */
    public function __construct(
        /** @var IdentificationUtilisateurContract */
        protected IdentificationUtilisateurContract $identification)
    {} 

    /**
     * @param string $identifiant
     * @param string $motDePasse
     * 
     * @return array
     * 
     */
    public function lireMessages(string $identifiant, string $motDePasse): array 
    {
        if ($this->identification->identifier($identifiant, $motDePasse)) 
        {
            return [];
        } 
        
        throw new IdentificationException();
    }
}
```

Le fichier *IdentificationException.php* contient le code suivant:
``` php
<?php

namespace Maxicoffee\ExampleTests\Exceptions;

class IdentificationException extends \Exception
{
    /**
     * @param string $message
     * @param  $code
     * @param \Throwable|null $previous
     *
     */
    public function __construct(string $message = 'Identification Utilisateur en échec.', int $code = 403, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
```

Enfin, le fichier *composer.json* initialise le projet avec le code suivant:
```json
{
    "name": "maxicoffee/exampletests",
    "type": "project",
    "authors": [
        {
            "name": "Qualite SI",
            "email": "qualite.si@maxicoffee.com"
        }
    ],
    "autoload": {
        "psr-4": {
            "Maxicoffee\\ExampleTests\\": ["src/"]
        }
    },
    "autoload-dev": {
        "psr-4": {
            "Maxicoffee\\ExampleTests\\Tests\\": "tests/"
        }
    },
    "require": {},
    "require-dev": {
        "phpunit/phpunit": "^9.5"
    },
    "minimum-stability": "dev"
}
```

Pour commencer à tester le code de la messagerie utilisateur, rajoutons le fichier *MessagerieUtilisateurTests.php* dans le dossier **tests**, et remplissons le avec les lignes de code suivantes:
``` php
<?php

namespace Maxicoffee\ExampleTests\Tests;

use Maxicoffee\ExampleTests\Messagerie\MessagerieUtilisateur;
use PHPUnit\Framework\TestCase;

class MessagerieUtilisateurTest extends TestCase 
{
    /**
     * @covers MessagerieUtilisateur::lireMessages
     * @return void
     * 
     */
    public function testLireMessage(): void 
    {
        // problème : comment instancier messagerie, il nous manque un paramètre
        MessagerieUtilisateur $messagerie = new MessagerieUtilisateur(???);

        try {
            $messages = $messagerie->lireMessages('userStubbed', '4TestPurpose');
            $this->assertEquals([], $messages);
        } catch (IdentificationException $e) {
            $this->assertTrue(false);
        }

        try {
            $messagerie->lireMessages('userStubbed', '4TestOnly');
            // devrait renvoyer une exception IdentificationException
            $this->assertTrue(false);
        } catch (IdentificationException $e) {
            $this->assertTrue(true);
        }

    }
}
```

On se rend compte, dans le fichier *MessagerieUtilisateurTests.php*, que la ligne `MessagerieUtilisateur $messagerie = new MessagerieUtilisateur(???);` va poser un problème, attendu que l'autre équipe n'a pas pu nous fournir l'implémentation de l'interface *IdentificationUtilisateurContract*.

Dans ce cas précis, l'utilisation d'un bouchon va se substituer à la classe manquante le temps du test. Dans le dossier **tests**, rajoutons un fichier *IdentificationUtilisateurStub.php* avec le code suivant:
``` php
<?php

namespace Maxicoffee\ExampleTests\Tests;

use Maxicoffee\ExampleTests\Identification\IdentificationUtilisateurContract;

/**
 * doublure de la classe IdentificationUtilisateurContract
 * c'est un bouchon qui est utilisé dans MessagerieUtilisateurTests
 */
class IdentificationUtilisateurStub implements IdentificationUtilisateurContract 
{
    /**
     * seul le compte "userStubbed" avec le mot de passe "4TestPurpose" peut s'identifier
     *
     * @param string $identifiant
     * @param string $motDePasse
     * 
     * @return bool
     * 
     */
    public function identifier(string $identifiant, string $motDePasse): bool 
    {
        if ('userStubbed' === $identifiant && '4TestPurpose' === $motDePasse) {
            return true;
        }
        return false;
    }    
}
```

Enfin, dans le fichier *MessagerieUtilisateurTests.php*, il ne reste plus qu'à remplacer la ligne `MessagerieUtilisateur $messagerie = new MessagerieUtilisateur(???);` par la ligne `MessagerieUtilisateur $messagerie = new MessagerieUtilisateur(new IdentificationUtilisateurStub());`, et il est maintenant possible de dérouler le test, sans plus attendre. Le bouchon a permis de résoudre le problème de dépendance non-satisfaite envers un composant qui n'est pas disponible. 

**Remarque**: l'utilisation des lignes `assert`, dans le fichier *MessagerieUtilisateurTests.php*, marquent l'implémentation du pattern AAA (Arrange / Act / Assert). Ce pattern est un moyen couramment utilisé pour écrire les tests unitaires, et se traduit de la manière suivante:
- La section Arrange d'une méthode de test unitaire initialise les objets et définit la valeur des données transmises à la méthode testée. 
- La section Act appelle la méthode testée avec les paramètres définis.
- La section Assert vérifie que l'action de la méthode testée se comporte comme prévu en comparant le résultat obtenu au résultat attendu défini.

## LES SIMULACRES (MOCKS)
Contrairement aux bouchons, les simulacres ne sont pas écrits par le développeur mais générés par l'usage d'un outil dédié, qui permet de générer un simulacre à partir de la classe originale. Le simulacre généré est d'un type similaire à la classe originale et peut donc la remplacer. On peut ensuite, par programmation, définir comment la doublure doit réagir quand elle est manipulée par la classe testée.

En PHP, plusieurs outils sont disponibles pour créer des simulacres, on peut notamment citer le MockBuilder intégré à la classe TestCase de PHPUnit, ou encore Mockery (Documentation disponible [ici](https://github.com/mockery/mockery) et [là](http://docs.mockery.io/en/latest/)). Pour les besoins de cet exemple, nous utiliserons le mockBuilder intégré à PHPUnit.

<div align="center">

## EXEMPLE DE SIMULACRE / MOCK

</div>

Reprenons l'exemple précédent à son état initial, mais cette fois, nous allons préférer l'utilisation d'un simulacre à l'écriture du bouchon. Dans le dossier **tests**, rajoutons le fichier *MessagerieUtilisateurTests.php*, avec le code suivant:
``` php
<?php

namespace Maxicoffee\ExampleTests\Tests;

use Maxicoffee\ExampleTests\Messagerie\MessagerieUtilisateur;
use Maxicoffee\ExampleTests\Exceptions\IdentificationException;
use Maxicoffee\ExampleTests\Identification\IdentificationUtilisateurContract;
use PHPUnit\Framework\TestCase;

class MessagerieUtilisateurTest extends TestCase 
{
    /**
     * @covers MessagerieUtilisateur::lireMessages
     * @return void
     * 
     */
    public function testLireMessage(): void 
    {
       $identification = $this->getMockBuilder(IdentificationUtilisateurContract::class)
            ->setMethods([
                'identifier',
                ])
            ->getMock(); 
        $identification->method('identifier')
            ->will($this->returnCallback(function(string $identifiant, string $motDePasse) {
                if ('userMocked' === $identifiant && '4TestPurpose' === $motDePasse) {
                    return true;
                }

                throw new IdentificationException();
            }));
        
        $messagerie = new MessagerieUtilisateur($identification);

        try {
            $messages = $messagerie->lireMessages("userMocked", "4TestPurpose");
            $this->assertEquals([], $messages);
        } catch (IdentificationException $e) {
            $this->assertTrue(false);
        }

        try {
            $messagerie->lireMessages('Test 2', 'mauvais_mot_de_passe');
            $this->assertTrue(false);
        } catch (IdentificationException $e) {
            $this->assertTrue(true);
        }
    }
}
```

**Remarque**: l'utilisation de getMockBuilder permet de mocker un objet du type nécessaire, en surchargeant la méthode au travers d'une fonction callBack. Le simulacre est directement intégré dans le test, inutile donc de créer un nouveau fichier.

## LES ESPIONS (SPIES)
Un espion est une doublure qui enregistre les traitements qui lui sont fait. Les tests se déroulent alors ainsi :

- On introduit une doublure dans la classe testée
- On lance le traitement testé
- Une fois finie, on inspecte la doublure pour savoir ce qu'il s'est passé pour elle. On peut vérifier qu'une méthode a bien été appelée, avec quels paramètres et combien de fois etc.

L'intérêt de cette méthode est manifeste lorsqu'il s'agit de tester des méthodes privées. On ne peut vérifier la valeur retournée ou que les paramètres passés successivement au travers des différentes méthodes sont bons. Dans ce cas, on laisse l'espion passer successivement pour toutes les méthodes privées et c'est seulement à la fin que l'espion nous rapporte ce qu'il s'est vraiment passé.

Une classe espionne peut être codée à la main mais la plupart des outils de création de simulacres créent des doublures qui sont aussi des espions. 

<div align="center">

## EXEMPLE D'ESPION / SPY

</div>

Reprenons les tests tels que décrient à la fin de la section précédente. Un objet simulacre est bien généralement un objet espion également.
``` php
<?php

namespace Maxicoffee\ExampleTests\Tests;

use Maxicoffee\ExampleTests\Messagerie\MessagerieUtilisateur;
use Maxicoffee\ExampleTests\Exceptions\IdentificationException;
use Maxicoffee\ExampleTests\Identification\IdentificationUtilisateurContract;
use PHPUnit\Framework\TestCase;

class MessagerieUtilisateurTest extends TestCase 
{
    /**
     * @covers MessagerieUtilisateur::lireMessages
     * @return void
     * 
     */
    public function testLireMessage(): void 
    {
       $identification = $this->getMockBuilder(IdentificationUtilisateurContract::class)
            ->setMethods([
                'identifier',
                ])
            ->getMock(); 
        $identification
            ->expects($this->once())
            ->method('identifier')
            ->will($this->returnCallback(function(string $identifiant, string $motDePasse) {
                if ('userMocked' === $identifiant && '4TestPurpose' === $motDePasse) {
                    return true;
                }

                throw new IdentificationException();
            }));            

        $messagerie = new MessagerieUtilisateur($identification);

        try {
            $messages = $messagerie->lireMessages("userMocked", "4TestPurpose");
            $this->assertEquals([], $messages);
        } catch (IdentificationException $e) {
            $this->assertTrue(false);
        }

        try {
            $messagerie->lireMessages('Test 2', 'mauvais_mot_de_passe');
            $this->assertTrue(false);
        } catch (IdentificationException $e) {
            $this->assertTrue(true);
        }
    }
}
```

**Remarque**: le rajout de la ligne `->expects($this->once())` a permis de vérifier que l'appel à l'identification n'est réalisée qu'une fois. Ainsi, le rapport de PHPUnit indique que le premier test passe bien, alors que le second indiquant le texte d'un échec "was not expected to be called more than once".
Pour faire passer le second test au statut OK, il suffit de remplacer la ligne `->expects($this->once())` par `->expects($this->exactly(2))`.
``` php
<?php

namespace Maxicoffee\ExampleTests\Tests;

use Maxicoffee\ExampleTests\Messagerie\MessagerieUtilisateur;
use Maxicoffee\ExampleTests\Exceptions\IdentificationException;
use Maxicoffee\ExampleTests\Identification\IdentificationUtilisateurContract;
use PHPUnit\Framework\TestCase;

class MessagerieUtilisateurTest extends TestCase 
{
    /**
     * @covers MessagerieUtilisateur::lireMessages
     * @return void
     * 
     */
    public function testLireMessage(): void 
    {
       $identification = $this->getMockBuilder(IdentificationUtilisateurContract::class)
            ->setMethods([
                'identifier',
                ])
            ->getMock(); 
        $identification
            ->expects($this->exactly(2))
            ->method('identifier')
            ->will($this->returnCallback(function(string $identifiant, string $motDePasse) {
                if ('userMocked' === $identifiant && '4TestPurpose' === $motDePasse) {
                    return true;
                }

                throw new IdentificationException();
            }));            

        $messagerie = new MessagerieUtilisateur($identification);

        try {
            $messages = $messagerie->lireMessages("userMocked", "4TestPurpose");
            $this->assertEquals([], $messages);
        } catch (IdentificationException $e) {
            $this->assertTrue(false);
        }

        try {
            $messagerie->lireMessages('Test 2', 'mauvais_mot_de_passe');
            $this->assertTrue(false);
        } catch (IdentificationException $e) {
            $this->assertTrue(true);
        }
    }
}
```

## LES SUBSTITUTS (FAKES)
Un substitut est une doublure écrite à la main qui implémente le comportement attendu d'une classe mais de façon plus simple. Contrairement au bouchon qui est écrit spécifiquement pour un test, le substitut a vocation à être suffisamment générique pour être utilisé dans plusieurs tests. Il est donc plus compliqué à écrire que le bouchon mais a l'avantage d'être réutilisable. Le temps supplémentaire passé à écrire cette doublure peut être rentabilisé si elle est réutilisée dans d'autres tests.

<div align="center">

## EXEMPLE DE SUBSTITUT / FAKE

</div>

Revenons à notre premier exemple, celui du bouchon. Notre test est très court, supposons que nous avons d'autres tests à mener sur notre classe MessagerieUtilisateur. Par exemple, on voudrait tester le comportement de la classe avec plusieurs utilisateurs différents, et vérifier que les messages sont bien stockés. Nous avons la possibilités de créer d'autres bouchons pour chacun de ces tests, mais essayons plutôt d'écrire un substitut qui pourra servir dans chaque test.

Ainsi, le code dans le fichier *IdentificationUtilisateurStub.php* devient:
``` php
<?php

namespace Maxicoffee\ExampleTests\Tests;

use Maxicoffee\ExampleTests\Exceptions\IdentificationException;
use Maxicoffee\ExampleTests\Identification\IdentificationUtilisateurContract;

/**
 * doublure de la classe IdentificationUtilisateurContract
 * c'est un bouchon qui est utilisé dans MessagerieUtilisateurTests
 */
class IdentificationUtilisateurStub implements IdentificationUtilisateurContract 
{

    /**
     * @var array
     */
    private array $comptes = [];

    /**
     * seul le compte "userStubbed" avec le mot de passe "4TestPurpose" peut s'identifier
     *
     * @param string $identifiant
     * @param string $motDePasse
     * 
     * @return bool
     * 
     */
    public function identifier(string $identifiant, string $motDePasse): bool 
    {
        if (array_key_exists($identifiant, $this->comptes)) {
            if ($this->comptes[$identifiant] === $motDePasse) {
                return true;
            }
        }

        throw new IdentificationException();
    }

    /**
     * ajouter un compte à l'ensemble des comptes valides
     *
     * @param string $identifiant
     * @param string $motDePasse
     * 
     * @return void
     * 
     */
    public function ajouterCompte(string $identifiant, string $motDePasse): void
    {
        $this->comptes[$identifiant] = $motDePasse;
    }
}
```

## LES FANTOMES (DUMMIES)
Il s'agit du type de doublure le plus simple, il s'agit simplement d'un objet qui ne fait rien puisqu'on sait que, de toute façon, dans la méthode testée, il n'est pas utilisé. Il y a plusieurs façons simples de créer un fantôme :
- Il peut s'agir d'un objet qui implémente une interface et laisse toutes les implémentations vides
- Un simulacre généré
- Un objet quelconque, par exemple une instance de Object en Java (on peut très bien faire Object fantome = new Object();)
- Parfois, l'absence de valeur convient très bien (null) 

## BENEFICES & INCONVENIENTS
|BENEFICES|INCONVENIENTS|
|:---|:---|
|- Découplage entre les classes <br>- Reproductibilité accrue <br>- Augmentation significative de la qualité des tests étant donné la possibilité de tester les situations exceptionnelles|- Temps de développement initial <br>- Temps de maintenance : nécessité pour les doublures de suivre les changements dans les classes originales <br>- Lorsqu'une classe fonctionne avec une doublure, cela ne garantit pas que la classe fonctionnera quand il s'agira d'interagir avec la véritable implémentation. Pour vérifier cette seconde propriété, il faudra recourir à un test d'intégration.|

Source: https://fr.wikibooks.org/wiki/Introduction_au_test_logiciel/Doublures_de_test

## LE PATTERN AAA
Evoqué plus haut au paragraphe concernant les bouchons, le Pattern AAA (Arrange / Act / Assert) s'applique couramment quand il s'agit des tests unitaires. Cela étant dit, il est à noter qu'il s'applique aussi parfaitement sur d'autres types de tests, notamment les tests de comportements que l'on retrouve dans la philosophie BDD (Behaviour Driven Development, Développement Piloté par les Comportements).

De la forme Etant donné que (Given) ..., Quand (When) ..., Alors (Then) ..., on peut aisément faire les rapprochements entre la section Arrange et le mot clé Given, entre la section Act et le mot clé When, et  enfin entre la section Assert et le mot clé Then.

Le framework utilisé en PHP pour construire des scénarios de Behaviour Driven Development s'appelle Behat.

Ainsi, un test écrit en AAA comme ci-dessous:
``` php
    public function testLireMessage(): void 
    {
        $messagerie = new MessagerieUtilisateur(new IdentificationUtilisateurStub());

        try {
            $messages = $messagerie->lireMessages("userStubbed", "4TestPurpose");

            $this->assertEquals([], $messages);

        } catch (IdentificationException $e) {
            $this->assertTrue(false);
        }

        try {
            $messagerie->lireMessages('Test 2', 'mauvais_mot_de_passe');
            $this->assertTrue(false);
        } catch (IdentificationException $e) {
            $this->assertTrue(true);
        }
    }
```
pourrait correspondre à une Feature et deux scénarios de BDD décrit comme ceci:
```
Feature: Messagerie
    As a user who wants to read his messages

    Scenario: A valid user wants to read his messages with no messages in box
        Given a user has a valid account
        When a user wants to read his messages with 'Utilisateur Behat' for login and '4BehatPurpose' for password
        Then the messages count is 0
        And no message is displayed

    Scenario: An invalid user wants to read his messages
        Given a user does not have a valid account
        When a user wants to read his messages with 'Utilisateur inconnu' for login and 'Try it yourself' for password
        Then the error message 'Identification Utilisateur en échec.' is displayed
```
et pourrait se traduire de la manière suivante en terme de code:
``` php
<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use Maxicoffee\ExampleTests\Tests\IdentificationUtilisateurStub;
use Maxicoffee\ExamplesTests\Identification\IdentificationUtilisateurContract;
use Maxicoffee\ExamplesTests\Messagerie\MessagerieUtilisateur;

/**
 * Classe de contexte pour les runs des scénarios
 * BDD et Behat
 */
class FeatureContext implements Context 
{
    /** @var MessagerieUtilisateur */
    private MessagerieUtilisateur $messagerie;
    /** @var array */
    private array $messages;
    /** @var string */
    private string $errorMessage = '';

    /**
    * @Given a user has a valid account
    */
    public function aUserHasAValidAccount() 
    {
        $this->identification = new IdentificationUtilisateurStub();
        $this->identification->ajouterCompte('Utilisateur Behat', '4BehatPurpose');
        $this->identification->ajouterCompte('Behat Utilisateur', '4BehatOnly');

        $this->messagerie = new MessagerieUtilisateur($this->identification);
    }

    /**
     * @When a user wants to read his messages with :login for login and :password for password
     */
    public function aUserWantsToReadHisMessagesWithLoginAndPassword(string $login, string $password) 
    {
        try {
            $this->messages = $this->messagerie->lireMessage($login, $password);
        } catch (\Exception $e) {
            $this->errorMessage = $e->getMessage();
        }                
    }

    /**
     * @Then the messages count is :numberOfMessages
     */
    public function theMessagesCountIs(int $numberOfMessages) 
    {
        $nbMessagesReceived = count($this->messages);

        if ($numberOfMessages !== $nbMessagesReceived) {
            throw new \Exception(" Number of Messages expected ($numberOfMessages) is not equal to Number of Messages received ($nbMessagesReceived)");
        }
    }

    /**
     * @Then no message is displayed
     */
    public function noMessageIsDisplayed() 
    {
        string $allMessages = '';

        foreach($this->messages as $message) {
            $allMessages .= "\n" . $messages;
        }
 
        if ($allMessages !=== '') {
            throw new \Exception("Found messages where there shouldn't be any, here are the messages: \n $allMessages");
        }
    }

    /**
     * @Then the error message :expectedError is displayed
     */
    public function theErrorMessageIsDisplayed(string $expectedError) 
    {
        string $exceptionReceived = $this->errorMessage;

        if ($expectedError !=== $exceptionReceived) {
            throw new \Exception("Expected Exception ($expectedError) is not equal to Exception Received ($exceptionReceived))");
        }
    }
}
```
Les trois principales différences entre le pattern AAA et l'approche BDD sont:
- Les étapes de scénario en BDD sont pensées pour être réutilisables au sein des différents scénarios, le code est donc factorisé (dans l'exemple ci-dessous, l'étape When est la même dans les deux scénarios, seules les données changent).
- Les assertions (en php / Behat du moins) sont inversées, on considère l'étape comme automatiquement validée, à moins que le contraire ne soit spécifiquement précisé.
- De plus haut niveau, les tests BDD sont des tests d'intégration et de service, ils n'ont pas la granularité des tests unitaires, et visent à valider un comportement attendu en terme de fonctionnalités. Les tests éprouvent donc plus aisément les services fournis que les unités de code au sens propre.

</div>

# 6. Quand écrire un test: le TDD <a name="when_write_tdd"></a>
<div style="text-align: justify">

Il n'y a pas exactement de bonne réponse à cette question. Cela étant dit, il est important de garder un point en tête quand il s'agit d'écrire des tests à posteriori du code, parfois des mois voire des années après l'écriture du code. Lorsque l'on écrit un test à posteriori du code, bien souvent, le test décrit le résultat attendu par le code testé, et non pas le résultat fonctionnel attendu à l'origine.

Pour autant, mieux vaut encore rajouter ces tests sur un projet qui n'en a pas que de le laisser sans aucune couverture de tests. Ce n'est donc pas inutile de les écrire, et cela peut permettre de démarrer de nouvelles habitudes à pérenniser.

De fait, s'il n'y a pas de bonne réponse, il y a toutefois une bonne pratique: le Test Driven Development (ou TDD), le Développement Piloté par les Tests.
Le TDD est une pratique qui consiste à écrire en premier lieu et en priorité un test, puis ensuite d'écrire le code correspondant.

Cela présente plusieurs avantages :
- Le test décrit la fonctionnalité (et non pas le code) et son résultat attendu. Le code devra donc répondre à cette demande, ce qui aide à clarifier le raisonnement pour l'écriture du code à proprement parler.
- Cela incite à employer la méthode RGR pour Red Green Refactor 
    - Le code n'existant pas, le test tombe forcément en échec: RED
    - Une fois le code écrit, le test est validé: Green
    - On en profite pour simplifier et optimiser test et code: Refactor
- Le test étant écrit avant le code, la couverture du code par les tests est optimale. Cela facilite entre autre la surveillance des non-régressions.
- Le test étant écrit avant le code, cela aide aussi à garder le code produit dans sa plus simple expression minimale et nécessaire pour valider le test.
</div>

# 7. Le kata Bowling pour s'initier au TDD <a name="bowling_kata_tdd"></a>
<div style="text-align: justify">

<div align="center">

## BOWLING GAME KATA

</div>

> **A TOUJOURS GARDER EN TETE:** 
> - **RED: Ecrire le test en premier,** 
> - **GREEN: Ecrire le code qui fait passer le test,**
> - **REFACTOR: Refactoriser les tests ou le code (simplifier la lecture et/ou optimiser le code)**
> 
> Ce kata a pour objectif de favoriser l'assimilation de la philosophie inhérante au Test Driven Development (TDD), au Simple Design, aux Tests Unitaires, ...
> 
> **Chaque étape du processus d'écriture de code peut amener une solution différente, selon la logique employée.**
> 
> **A faire lentement, à prendre le temps de la réflexion, à répéter.**

<div align="center">

## REGLES DE SCORING AU BOWLING

</div>

> L’objectif de cet exercice est d’implémenter un composant qui calcule le score d’une partie de bowling américain.
> 
> Une partie se déroule en 10 tours. À chaque tour, le joueur dispose de deux essais pour faire tomber 10 quilles disposées en triangle. Le score de base est le nombre de quilles que le joueur a réussi à faire tomber pendant le tour.
> 
> Lorsque le joueur arrive à faire tomber les dix quilles en deux lancers, cela  s’appelle un spare. Le score d’un spare est de 10, auquel on ajoute le nombre de  quilles tombées au lancer suivant.
> 
> Lorsque le joueur arrive à faire tomber les dix quilles en un seul lancer, cela  s’appelle un strike. Le score d’un strike est de 10, auquel on ajoute le total de  quilles tombées aux deux lancers suivants.
> 
> Lors du dernier tour:
> - si le joueur réalise un spare, il dispose d’un lancer bonus pour permettre de  calculer son score ;
>  - si le joueur réalise un strike, il dispose de deux lancers bonus pour permettre de calculer son score.
> 
> Cela signifie qu’une partie de bowling se termine en 21 lancers maximum (10 spare d’affilée).
> 
> Le composant que l’on cherche à implémenter n’est pas responsable de valider le nombre de coups joués dans une partie ni que les coups sont "légaux". On part de l’hypothèse que cette validation est réalisée avant d’interagir avec ce composant, et donc qu’il sera systématiquement appelé avec une entrée bien formée.

(Source: [ZesteDeSavoir - Kata du Bowling Game](https://zestedesavoir.com/billets/3951/le-kata-du-bowling-game/))

</div>

# 8. Pour aller plus loin: une réflexion sur quoi tester <a name="tests_questioning"></a>
<div style="text-align: justify">

Il a été fait mention plus tôt dans cette formation de la représentation sous forme de pyramide pour les tests, ou sous sa forme opposée en cône de crême glacée.

Une autre représentation possible est ce que l'on appelle les Quadrants de Tests.
En résumé, cela répartit la stratégie de tests selon 4 axes d'études:
- Axe vertical:
    - Tests Fonctionnels
    - Tests Non Fonctionnels
- Axe horizontal:
    - Aider l'équipe
    - Challenger le produit

Pour en savoir plus sur ces quadrants, la [vidéo de  Brevitatis](https://www.youtube.com/watch?v=jny4rGj4w88) est très bien faite, et explique les liens entre les quadrants et la pyramide.

La chaine [Test Academy](https://www.youtube.com/watch?v=29S9vyfgDEk&list=PLmhQGrnqvRQxHbtA_AG0S_VQWJZS1zblN) sur youtube propose également une séquence de vidéos détaillant les 7 principes du test.

Enfin, pour bien comprendre qu'une stratégie de tests reste malgré tout propre à l'entreprise et à ses projets, Jonathan Boccara nous propose, lors d'un [talk Devoxx FR](https://www.youtube.com/watch?v=QL0HBeIAny0), de nous expliquer la politique que Doctolib a mis en place par rapport à ses tests, s'affranchissant du modèle en pyramide pour une approche plus personnalisée.
</div>

# Table des matières
1. [De quoi parle-t-on ?](#what_is_bdd)
2. [L'approche BDD](#bdd_philosophy)
3. [Le langage Gherkin](#gherkin_introduction)
4. [Idées reçues](#stereotypes)
5. [Quelques bonnes pratiques](#best_practices)
6. [Les outils pour automatiser les tests](#testing_tools)
    1. [PHP / Framework Behat](#php_behat)

# 1. De quoi parle-t-on ? <a name="what_is_bdd"></a>
<div style="text-align: justify">
Le Behaviour Driven Development (BDD, Développement Piloté par les Comportements) a été imaginé par Dan North en 2006 pour adapter la pratique du TDD (Tests Driven Developement, Développement Piloté par les Tests) au niveau du métier.

Ce formalisme, appelé Gherkin, permet d'exprimer simplement les comportements attendus, de sorte que tout le monde, parmi les développeurs et les parties prenantes, ait la même compréhension du besoin formulé.

De fait, le BDD en lui-même n'est pas une méthode d'écriture des tests automatisés, mais plutôt un moyen pour l'équipe technique et le métier de discuter à l'aide d'un langage accessible commun. 
</div>

# 2. L'approche BDD <a name="bdd_philosophy"></a>
<div style="text-align: justify">

Il est vrai qu'un des intérêts majeurs des suites de tests automatisés est d'assurer la non-régression du code, qu'il soit technique ou fonctionnel.  

Ainsi, à chaque développement validé, l'ensemble des tests de tout niveau permet d'assurer un filet de sécurité quant aux codes sources ajoutés et/ou modifiés, de sorte que les fonctionnalités précédemment mises en place ne se retrouvent pas fragilisées par les nouveaux développements.  
  
Pour autant, la pratique des Tests automatisés permet d'aller plus loin que ce filet de sécurité. Ces Tests ont également un rôle de documentation par rapport à l'application, et pour les Tests orientés fonctionnels, ils représentent la garantie d'être conforme aux besoins en fonctionnalité exprimés par les métiers ou clients.

C'est d'autant plus vrai avec le BDD, dont les Features et Scenarios sont prévues pour être rédigés en commun avec toutes les parties concernées.

L'écriture des scénarios se fait de manière collective (développeurs, clients, supports, infrastructures, ...), tout le monde peut participer à l'expression du besoin puisque celui-ci se fait en langage naturel.

Les scénarios sont accompagnés de données d'exemple, des cas d'utilisation illustrés par des exemples concrets, rendant les règles métiers plus concrètes, et donc mieux comprises.

Les comportements fonctionnels attendus forment une documentation effective de l'application, documentation qui est par nature toujours le plus à jour possible, et disponible pour tous et toutes.

Facilitant le dialogue et la compréhension mutuelle sur les besoins exprimés, cela restaure également la confiance entre les parties, puisque le comportement attendu est explicitement visible.

Enfin, en se focalisant sur la dimension métier, il en ressort moins de digression, la technique étant à dessein évincée de la discussion. En effet, les choix techniques et d'implémentation devraient relever de la responsabilité pleine et entière des équipes techniques, et ne devraient donc pas perturber les discussions sur les besoins fonctionnels.
</div>

# 3. Le langage Gherkin <a name="gherkin_introduction"></a>
<div style="text-align: justify">

Si Gherkin n'est pas la seule option en matière de BDD, ce langage en est cependant le plus connu, le plus répandu, et donc le plus utilisé.  

Les outils dédiés, dans les différents langages de programmation pour exploiter l'approche BDD et ces spécifications fonctionnelles, s'appuient en conséquence bien souvent sur la syntaxe Gherkin.

Syntaxe donc qui a pour objectif d'écrire les spécifications fonctionnelles dans un langage naturel, compréhensible par l'ensemble des parties prenantes, et dont le format, en français, est constitué des mots clés principaux suivants:

- **Etant donné que** {Contexte}
- **Quand** {Evènement}
- **Alors** {Résultat - Eléments à vérifier}

Un exemple rapide est la gestion d'un panier d'achat sur un site e-commerce.  

> Scénario: un panier vide
> - Etant donné que le panier client est vide
> - Quand l'utilisateur affiche le panier client
> - Alors le prix affiché est 0

On remarque tout de suite que l'implémentation technique n'est ni décrite, ni suggérée, on ne décrit que le comportement métier attendu.

Cet aspect sur l'absence d'éléments techniques ne se voit pas forcément sur un concept de panier d'achat. Un second exemple, concernant une authentification, peut être plus parlant:  
> Scénario: connexion utilisateur
> - Etant donné que l'utilisateur n'est pas identifié
> - Quand l'utilisateur s'identifie sur le système
> - Alors l'utilisateur est connecté 
> - Et l'utilisateur a accès aux éléments de la plateforme

Dans cet exemple, il n'est fait aucune mention de la méthode d'authentification. L'utilisateur s'identifie-t-il avec un couple login/mot de passe ? Y a-t-il un système de reconnaissance digitale ? Ou l'utilisateur profite-t-il d'un système d'authentification automatique grâce à un Active Directory ?
Ces questions se poseront certainement pendant l'implémentation technique de la fonctionnalité, mais le besoin initial réel de l'utilisateur est "**Je souhaite pouvoir m'identifier sur la plateforme**".  

*Remarque: pour la suite de ce document, les exemples en Gherkin seront donnés avec les mots clés anglais.*

Maintenant que la précision concernant l'absence de l'implémentation technique est faite, voyons comment se formalise le besoin grâce au langage Gherkin.

En premier lieu, la rédaction d'une **Feature** en langage Gherkin doit suivre le principe suivant:
> "Une personne ne connaissant rien au projet doit pouvoir comprendre le comportement décrit, sans se poser de question, ni avoir de doute sur son interprétation."

A ce principe général s'ajoute une charte qui comprend 3 entrées:
1. Un découpage clair
    - 1 Fonctionnalité = 1 Fichier
    - 1 Scénario = 1 Comportement de la Fonctionnalité
2. Une syntaxe claire et lisible
    - **Given** + **When** + **Then**, éventuellement complétés par des conditions supplémentaires avec les mots clés **And** et/ou **But**.
    - Respecter l'ordre **Given** > **When** > **Then** afin que la rédaction suive l'ordre chronologique du comportement utilisateur attendu.
    - Un double espace ou une tabulation entre chaque niveau et niveau enfant (**Feature** > **Scenario** > **Step**).
3. Des titres de scénarios clairs et synthétiques
    - Pour vérifier rapidement la pertinence du scénario.
    - Pour identifier les scénarios en double.
    - Pour identifier facilement les scénarios à modifier.

Les mots clés principaux utilisés par le langage Gherkin sont décrits dans le tableau suivant:

|KEYWORD|MOT CLE|POSITION|DESCRIPTION|
|:---|:---|:---|:---|
|Feature|Fonctionnalité|1ère ligne du fichier|Suivi du caractère `:` puis du nom de la fonctionnalité|
|Scenario|Scénario|1ère ligne d'un scénario, <br>décalage 1|Suivi du caractère `:` puis du nom du scénario|
|Given|Etant <br>donné que|1ère ligne étape d'un scénario, <br>décalage 2|Suivi de la phrase descriptive de l'étape|
|When|Quand|2ème ligne étape d'un scénario, <br>décalage 2|Suivi de la phrase descriptive de l'étape|
|Then|Alors|3ème ligne étape d'un scénario, <br>décalage 2|Suivi de la phrase descriptive de l'étape|
|And|Et|Après une étape **Given**, <br>**When**, et/ou **Then**| Suivi de la phrase descriptive de l'étape complémentaire|
|But|Mais|Après une étape **Given**, <br>**When**, et/ou **Then**| Suivi de la phrase descriptive de l'étape complémentaire|

Il existe d'autres mots clés dans la syntaxe Gherkin, optionnels, on y retrouve notamment:

|KEYWORD|MOT CLE|POSITION|DESCRIPTION|
|:---|:---|:---|:---|
|Rule|Règle|Entre la description de la **Feature** <br>et un ensemble de scénarios|Permet de regrouper les scénarios <br>en fonction d'une règle métier spécifique|
|Background|Contexte|Juste avant le premier scénario|Permet de définir un ensemble de situations initiales <br>communes à chacun des scénarios de la **Feature**|

</div>

# 4. Idées reçues<a name="stereotypes"></a>
<div style="text-align: justify">

- **Le BDD n'est pas une méthode d'écriture de tests !**

Il s'agit d'une méthodologie dont le but est de s'assurer que l'ensemble des parties prenantes du développement logiciel partagent la même compréhension du comportement logiciel attendu. Pour cela, Product Owner (PO), Quality Assurance (QA), et développeurs (les 3 amigos) se mettent autour de la table afin de poser par l'exemple la manière dont le logiciel doit se comporter.

Il n'est pas question de tests ici mais bien de discussions. Cette confusion vient du fait que bien souvent, l'objectif secondaire de cette pratique est l'implémentation de tests automatisés répondant aux exigences des exemples écrits en session.
  
- **Utiliser Cucumber et Gherkin ne garantit pas d'appliquer correctement la méthode BDD !**

Gherkin est un Domain Specific Language (DSL), ici, un cadre à l'écriture des exemples que l'on appellera alors scénarios.

Il en existe d'autres, et il serait même possible à une équipe plus mature sur le sujet de concevoir son propre DSL pour répondre à des contraintes très spécifiques auxquelles Gherkin ne pourrait pas répondre.
Simplement, Gherkin étant largement utilisé et prêt à l'emploi, il facilite l'adoption de la méthode BDD.

Parmi les outils d'automatisation des tests, Cucumber (langage Ruby), qui en est l'un des premiers, exploite les scénarios écrits en Gherkin afin de permettre l'automatisation de tests.

La force du BDD vient des discussions que la méthode provoque, et les outils utilisés ne sont pas une fin en soi. Les tests dans cette méthode sont la cerise sur le gateau.

- **Un représentant des développeurs ne suffit pas !**

L'objectif intrinsèque du BDD étant d'arriver à une compréhension commune du comportement attendu, mettre à l'écart une partie de l'équipe entre donc en conflit avec cette philosophie.

Un développeur implémente le besoin tel qu'il le comprend. Et il y a autant de manières de comprendre un besoin qu'il y a de personnes dans l'équipe. Et donc autant d'opportunités de discuter pour affiner le besoin, poser des exemples.

Qui plus est, cela permet également de co-construire un langage unifié, de cette façon, tout le monde utilise le même vocabulaire, et personne n'est perdu dans l'incompréhension.

- **Le BDD n'est pas compliqué, mais les premiers essais peuvent s'avérer frustrants !**

Les discussions provoquées peuvent amener beaucoup de remise en question sur la connaissance actuelle du logiciel. Décortiquer son comportement nécessite un vocabulaire précis et contextualisé, souvent manquant au démarrage.

Il est fréquent que les premiers essais tournent principalement autour de la définition de ce vocabulaire spécifique. Il faut voir cela comme un investissement pour les sessions à venir, l'élaboration de ce dictionnaire commun permettra de fluidifier la conception des scénarios à venir.

Dans ce contexte, un atelier [Event Storming](https://cleandojo.com/2019/06/event-storming-modelisez-votre-domaine-metier-en-equipe/) peut s'avérer être une bonne synergie avec la mise en place du BDD.

</div>

# 5. Quelques bonnes pratiques<a name="best_practices"></a>
<div style="text-align: justify">

Il faut éviter de multiplier les tuples/paires de **Given**/**When**/**Then**.  
Un scénario bien pensé doit chainer les étapes dans l'ordre, sans qu'une étape **Given** se retrouve après une étape **When** ou **Then**, de même qu'une étape **When** ne devrait pas apparaître après une étape **Then**.

La formulation est importante, il est préférable d'écrire les étapes en tant que phrase sujet prédicat action, à la troisième personne du singulier, et au présent de l'indicatif.
Cela évite la confusion et uniformise l'ensemble.

Ci-dessous, une liste des bonnes pratiques de style concernant l'écriture en Gherkin.
```
1. Concentrer une fonctionnalité sur les besoins des clients.

2. Limiter une fonctionnalité par fichier de fonctionnalité. Cela facilite la recherche de fonctionnalités.

3. Limiter le nombre de scénarios par fonctionnalité. Personne ne veut d'un fichier de fonctionnalités de mille lignes. Une bonne mesure est une douzaine de scénarios par fonctionnalité.

4. Limiter le nombre d'étapes par scénario à moins de dix.

5. Limiter le nombre de caractères de chaque étape. Les limites courantes sont de 80 à 120 caractères.

6. Utiliser une orthographe correcte.

7. Utiliser une grammaire appropriée.

8. Mettre en majuscule les mots-clés Gherkin.

9. Mettre en majuscule le premier mot dans les titres.

10. Ne pas mettre les mots en majuscules dans les phrases d'étape à moins qu'ils ne soient des noms propres.

11. Ne pas utiliser de ponctuation (en particulier les points et les virgules) à la fin des phrases d'étape.

12. Utiliser des espaces simples entre les mots.

13. Indenter le contenu sous chaque en-tête de section.

14. Séparer les fonctionnalités et les scénarios par deux lignes vides.

15. Séparer les tableaux d'exemples par 1 ligne vide.

16. Ne pas séparer les étapes d'un scénario par des lignes vides.

17. Espacer uniformément les délimiteurs de table ("|").

18. Adopter un ensemble standard de noms de balises. 

19. Écrire tous les noms de balises en minuscules et utiliser des traits d'union ("-") pour séparer les mots.

20. Limiter la longueur des noms de balises.
```
</div>

# 6. Les outils pour automatiser les tests<a name="testing_tools"></a>
<div style="text-align: justify">

Maintenant que l'on a vu en quoi consiste la méthodologie BDD, et l'un des langages dédiés associés, le Gherkin, il est temps de voir comment cela peut se traduire du point de vue technique.

## 6.1. PHP / Framework Behat <a name="php_behat"></a>
Behat est un framework PHP permettant l'implémentation du BDD.

Dans le fichier composer.json du projet PHP, dans la section `require-dev`, rajouter la ligne `"behat/behat": "3.10.0".

***Remarque***: il faut s'assurer que la plateforme PHP est bien définie dans la section `config` (`"config": { "platform": { "php": "7.3" } },`).  
La version minimale de PHP supportée est la 5.3.

Une fois Behat installé, il faut l'initialiser avec la commande `./vendor/bin/behat --init`.

Par défaut, cela va créer de nouveaux répertoires et fichiers dans le projet:  
- Le dossier `./features` contient l'ensemble des fichiers **\*.features**
- Le dossier `./features/bootstrap` est le dossier par défaut qui contient les classes de contexte fonctionnel.
- Le fichier `./features/bootstrap/FeatureContext.php` est le point d'entrée par défaut des scénarios en mode tests, il contient les définitions, les transformations, et les accroches.

Il est possible de configurer Behat pour qu'il utilise plusieurs fichiers de contexte, au lieu du seul fichier `FeatureContext.php`.

Cela se fait à l'aide d'un fichier `Behat.yml` à la racine du projet, contenant les lignes suivantes:
```
# behat.yml  
  
default:  
    suites:  
        default:  
            contexts:  
                - FeatureContext  
                - SecondContext  
                - ThirdContext  
```

Les fichiers de contexte sont listés dans le tableau `contexts`.
Dans l'exemple ci-dessus, FeatureContext est le fichier créé par défaut, SecondContext et ThirdContext sont de nouveaux fichiers de contexte créés manuellement. Ils se trouvent tous dans le dossier `./features/bootstrap`.  
On pourrait donc facilement imaginer des fichiers UserContext, ProductContext, OrderContext, ... en fonction des besoins métiers.

***Remarque***: chacun des fichiers de contexte doit respecter la norme PSR-0 pour être autoloadé (le nom du fichier = le nom de la classe). Qui plus est, chaque classe de contexte doit implémenter l'interface `Behat\Behat\Context\Context` pour être exécutée.

Pour exécuter la suite de tests des scénarios, il faut saisir la commande `vendor/bin/behat`.

Le framework Behat est en mesure de proposer les squelettes des fonctions qu'il estime nécessaire pour explorer les différentes étapes des scénarios. Ces squelettes sont faciles à adapter et permettent de démarrer rapidement dans l'utilisation de Behat.  

Toutefois, il est possible de configurer manuellement le mapping entre Gherkin et le fichier de contexte. En effet, Behat utilise le formalisme suivant en commentaire de fonctions:
```
    /**
     * @Given Situation_Initiale_Decrite_En_Gherkin
     */
```
```
    /**
     * @When Evenement_Decrit_En_Gherkin
     */
```
```
    /**
     * @Then Resultat_Attendu_Decrit_En_Gherkin
     */
```

Il est également possible de passer des paramètres à ces mappings, ils s'écrivent sous la forme `:NomParamètre` dans la phrase du mapping, et portent le même nom en tant que paramètre de la fonction.

Enfin, il faut savoir qu'un scénario de test échoue dès lors qu'une exception est levée. Dans le cas contraire, le test est considéré validé.

## Exercice pratique
> Il était une fois une série de 5 livres sur un héros très anglais qui s'appelait Harry. (Au moins quand ce Kata a été inventé, il n'y en avait que 5. Depuis, ils se sont multipliés) Les enfants du monde entier pensaient qu'il était fantastique, et, bien sûr, l'éditeur aussi. Ainsi, dans un geste d'immense générosité envers l'humanité (et pour augmenter les ventes), ils ont mis en place le modèle de tarification suivant pour tirer parti des pouvoirs magiques de Harry. Un exemplaire de l'un des cinq livres coûte 8 EUR. Si, toutefois, vous achetez deux livres différents de la série, vous bénéficiez d'une remise de 5 % sur ces deux livres. Si vous achetez 3 livres différents, vous bénéficiez d'une réduction de 10 %. Avec 4 livres différents, vous bénéficiez d'une remise de 20%. Si vous achetez les 5, vous bénéficiez d'une énorme réduction de 25%. Notez que si vous achetez, disons, quatre livres, dont 3 sont des titres différents, vous bénéficiez d'une réduction de 10% sur les 3 qui font partie d'un ensemble, mais le quatrième livre coûte toujours 8 EUR. La folie Potter balaie le pays et les parents d'adolescents du monde entier font la queue avec des paniers remplis de livres Potter. Votre mission est d'écrire un morceau de code pour calculer le prix de n'importe quel panier d'achat imaginable, en regroupant les tomes par groupes de 5 pour appliquer les remises sur ces groupes. (version allégée du kata).

Le Kata Potter est un Kata dont l'objectif est de calculer le prix remisé d'une suite de livres, en fonction des règles suivantes:

1. Un livre coûte 8 euros
2. L’achat de 1 livre n’apporte aucune réduction
3. L’achat de 2 livres différents apporte une réduction de 5%
4. L’achat de 3 livres différents apporte une réduction de 10%
5. L’achat de 4 livres différents apporte une réduction de 20%
6. L’achat de 5 livres différents apporte une réduction de 25%
7. Lors de l’achat d’un ensemble de livres, et pour avoir la plus petite réduction, ces livres sont regroupés dans différents lots auxquels on applique la réduction appropriée.

En utilisant la méthode BDD, écrivez la ou les fonctionnalités attendues, puis créez les fonctions pour explorer les scénarios, et enfin créez un code métier qui répond à la problématique.  
Astuce: pilotez les développements à la fois par les comportements attendus et par les tests. Faites des incréments successifs des comportements, et suivez le paradigme **RED** > **GREEN** > **REFACTOR**: 
- **RED**: la première étape, c'est la rédaction d'un test. Le code correspondant n'existe pas encore donc ce test va échouer et donc rester rouge.
- **GREEN**: la seconde étape, c'est l'écriture du code. Dans cette étape, on ne se concentre que sur une seule chose : faire passer notre test.
- **REFACTOR**: dernière étape, on refactorise notre code et nos tests. L'objectif ici est d'avoir un œil attentif aux améliorations que nous pouvons faire. Est-ce qu'il y a du code dupliqué ? Est-ce que ma classe a bien une seule responsabilité ? Est-ce que ma méthode fait bien ce que son nom indique qu'elle fait ?

Pour vous aider, voici les deux premiers scénarios:
```
    Scenario: Empty basket
        GIVEN an empty basket
        THEN the basket price is 0 euro

    Scenario: Basket with one book
        GIVEN an empty basket
        WHEN user adds 1 "Philosopher Stone" book inside basket
        THEN the basket price is 8 euros
```

> Pour l'exemple, combien coûte ce panier de livres ?
> - 2 exemplaires du premier livre
> - 2 exemplaires du second livre
> - 2 exemplaires du troisième livre
> - 1 exemplaire du quatrième livre
> - 1 exemplaire du cinquième livre

</div>
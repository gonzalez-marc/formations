# Table des matières
1. [Installation](#installation)
2. [Premier Contact](#first-contact)
    1. [Node.js](#nodejs)
    2. [Mariadb](#mariadb)
3. [Démystification des containers](#containers-demystification)
    1. [Définition](#containers-definition)
    2. [Cgroups](#cgroups)
    3. [Namespaces](#namespaces)
    4. [Machines virtuelles VS Containers](#vms-vs-containers)
    5. [Architecture Microservices](#microservices)
4. [Et le DevOps dans tout ça ?](#devops)
5. [L'architecture Docker](#docker)
    1. [Client / Serveur](#client-server)
    2. [Concepts essentiels](#main-concepts)
6. [Les containers](#containers)
    1. [Création](#containers-creation)
    2. [Mode intéractif](#interactive-mode)
    3. [Foreground vs Background](#foreground-vs-background)
    4. [Publication de port](#port-publication)
    5. [Bind Mount](#bind-mount)
    6. [Portainer](#portainer)
    7. [Limiter les ressources](#limit-resources)
    8. [Quelques commandes utiles](#usefull-cmd)
    9. [Commandes de base](#basic-cmd)
7. [Les images](#images)
    1. [Définition](images-definition)
    2. [AUFS FileSystem](#aufs)
    3. [Dockerfile](#dockerfile)
    4. [Principales instructions](#images-instructions)
8. [Stockage](#stockage)
    1. [Containers et persistance](#containers-persistance)
    2. [Les volumes](#volumes)
9. [Docker Compose](#docker-compose)
    1. [Présentation](#dc-presentation)
    2. [Docker-compose.yml](#dc-yml)
    3. [Le binaire docker-compose](#dc-binary)
10. [Network](#network)
    1. [Container Network Model](#network-ncm)
    2. [Cli](#network-cli)
    3. [Drivers](#network-drivers)
    4. [Networks d'un hôte Docker](#network-host)
    5. [Création d'un network bridge : user defined bridge](#network-bridge)
11. [Gestion des logs](#logs)
    1. [Persistance](#logs-persistance)
    2. [Drivers](#logs-drivers)
12. [Securité](#security)
    1. [Quelques rappels](#security-reminders)
    2. [Hardening](#security-hardening)
    3. [Docker Security Bench](#security-bench)
    4. [Linux Capabilities](#security-capabilities)
    5. [Linux Security Module](#security-module)
    6. [Secure Computing Mode](#security-scm)
    7. [Scanning de CVE](#security-cve)
    8. [Content trust](#security-content-trust)

# 1) Installation <a name="installation"></a>
L'installation de Docker se fait de manière aisée. La [documentation officielle](https://docs.docker.com/get-docker/) décrit parfaitement la procédure à suivre.

Tandis que [Windows](https://docs.docker.com/desktop/windows/install/) et [Mac](https://docs.docker.com/desktop/mac/install/) bénéficient d'une version graphique, les utilisateurs [Linux](https://docs.docker.com/engine/install/debian/) doivent s'assurer d'un certain nombre d'étapes avant de pouvoir bénéficier d'une installation pleinement fonctionnelle.

Il est à noter qu'il est préférable de suivre les [étapes optionnelles](https://docs.docker.com/engine/install/linux-postinstall/) si l'on souhaite pouvoir exécuter Docker de manière optimale.

Afin de pouvoir suivre convenablement cette formation, il vous sera également nécessaire d'installer [docker-compose](https://docs.docker.com/compose/install/)


# 2) Premier Contact <a name="first-contact"></a>
Généralement l'aventure Docker commence avec le [registry officiel](https://hub.docker.com/). Celui-ci regroupe l'ensemble des images qui serviront aux différents containers.

![Taxonomie de Docker](./images/taxonomy-of-docker-terms-and-concepts.png)

- **Le registry** est une bibliothèque regroupant un ensemble d'images (applications) qui seront utilisées par des instances (containers).

- **L'image** peut être assimilée à une machine virtuelle déjà configurée. Il s'agit d'un fichier statique, non modifiable. Elle renferme du code exécutable qui par essence sera isolé du reste du système.

- **Le container** est une instance de l'image que l'on peut démarrer, arrêter, déplacer ou supprimer via le cli ou l'api rest de Docker.

## 2.1 Node.js <a name="nodejs"></a>

Commençons par un premier exemple simple

```shell
docker container run -it node
```

```shell
Unable to find image 'node:latest' locally
latest: Pulling from library/node
07471e81507f: Pull complete 
c6cef1aa2170: Pull complete 
13a51f13be8e: Pull complete 
def39d67a1a7: Pull complete 
a8367252e08e: Pull complete 
5402d6c1eb0a: Pull complete 
c018bad89663: Pull complete 
ba9fd0742332: Pull complete 
9d775c430e09: Pull complete 
Digest: sha256:d6a8cd069ab740759394a9338dc4bc4b9b32b15209cccd0c7faa32d438b1076e
Status: Downloaded newer image for node:latest
Welcome to Node.js v16.11.1.
Type ".help" for more information.
> 
```


Docker va pull une première fois l'image à partir du registry [hub.docker.com](hub.docker.com) et instancier un container auquel il attribuera un id unique. On peut alors jouer avec l'interpréteur
```javascript
[0, 1, 2, 3].forEach(number => console.log(number))
```

Les images étant **tagguées**, la version utilisée étant implicitement la **latest**. Concrètement nous aurions pu écrire : 
```shell
docker container run -it node:latest
```

C'est un des avantages principaux de Docker : on peut très facilement basculer entre les différentes versions d'une application, afin par exemple de s'assurer de la rétrocompatibilité d'un code donné avec les différentes versions d'un langage donné.

```shell
docker container run -it node:14.1-alpine
```

```shell
Unable to find image 'node:14.1-alpine' locally
14.1-alpine: Pulling from library/node
cbdbe7a5bc2a: Pull complete 
0deddb4d0d4b: Pull complete 
b49b5c4238a4: Pull complete 
2021d4207c54: Pull complete 
Digest: sha256:00557fd939b1b87d0cd240c84aad62b77efc75146892f3e60117a6a696223f16
Status: Downloaded newer image for node:14.1-alpine
Welcome to Node.js v14.1.0.
Type ".help" for more information.
>
```

Dans le cas présent on utilise la version **14.1-alpine**. Notez l'usage du **-alpine** qui n'a rien d'anodin. En effet les images sont basées sur Ubuntu, Debian, Fedora, etc...ou encore [Alpine](https://www.alpinelinux.org/) ! Cette dernière est une distribution orientée sécurité et ultra légère ; ce qui en fait excellente base pour qui voudrait construire une image personnalisée.


## 2.2 Mariadb <a name="mariadb"></a>

Il est évidemment possible de lancer des applications plus complexes. Pour illustrer, démarrons une instance de [**mariadb**](https://hub.docker.com/_/mariadb).

```shell
docker container run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=phee8Wiengee_ti -d mariadb
```

```shell
Unable to find image 'mariadb:latest' locally
latest: Pulling from library/mariadb
7b1a6ab2e44d: Pull complete 
034655750c88: Pull complete 
f0b757a2a0f0: Pull complete 
5c37daf8b6b5: Pull complete 
b4cd9409b0f6: Pull complete 
dbcda06785eb: Pull complete 
a34cd90f184c: Pull complete 
fd6cef4ce489: Pull complete 
3cb89a1550ea: Pull complete 
df9f153bd930: Pull complete 
Digest: sha256:c014ba1efc5dbd711d0520c7762d57807f35549de3414eb31e942a420c8a2ed2
Status: Downloaded newer image for mariadb:latest
e8490f0bf1d9827649e255b84942de73a0b5ef4671ebf6b878151cdcb75c9b9f

```

Nous introduisons ici la notion de port; Comme on le ferait dans le cadre du **NAT**, nous redirigeons le **port 3306 de la machine hôte** vers le **port 3306 du container** afin d'exposer notre serveur au reste du monde.

Le **flag -e** permet de définir des variables d'environnement. Ainsi au bout du container, maraidb définira le mot de passe root.


Le **flag -d** est utilisé pour faire fonctionner le container en **arrière plan**.

Vérifions que le serveur est fonctionnel en nous connectant via [**Dbeaver**](https://dbeaver.io/) par exemple ou en utilisant le client cli de mariadb.

```shell
mysql -uroot -P 3306 -h 127.0.0.1
```


Tout est OK, notre container est accessible depuis la machine hôte. Nous pouvons donc envisager d'y connecter une image Wordpress par exemple ou tout outil nécessitant une base de données.

# 3) Démystification des containers <a name="containers-demystification"></a>


## 3.1 Définition<a name="containers-definition"></a>


Bien que l'on assimile souvent le container à une boîte noire, il ne faut pas oublier qu'il s'agit surtout d'un simple processus isolé du reste du système.

Celui-ci a **l'impression d'être seul** sur le système grâce aux **namespaces**, technologie d'isolation présente dans le **noyau linux**. Ses ressources peuvent être limitées grâce aux [**Cgroups**](https://fr.wikipedia.org/wiki/Cgroups), autre technologie présente dans le kernel.

Il est important de comprendre que l'on peut lancer plusieurs containers sur une même machine. Ils partagent alors le noyau de la machine hôte, contrairement à des machines virtuelles qui ont chacune un système d'exploitation différent.


## 3.2 Cgroups<a name="cgroups"></a>

Les cgroups sont une autre feature du noyau linux qui a pour but de limiter les ressources qu’un processus peut utiliser : RAM, CPU, [**I/O**](https://en.wikipedia.org/wiki/Input/output), Network.


## 3.3 Namespaces<a name="namespaces"></a>

Les [**namespaces**](https://en.wikipedia.org/wiki/Linux_namespaces) sont une fonctionnalité du kernel Linux permettant d'isoler un processus du reste du système, à limiter ce qu'il peut voir.

Il existe 6 namespaces différents :
- **Pid** donne à un processus la vision de lui même ainsi que celles de ses processus fils
- **Net** donne au processus sa propre stack réseau
- **Mount** permet de donner à un processus le système de fichiers sur lequeil il tourne
- **Uts** permet la gestion du nom de l'hôte
- **Ipc** assure la communication inter-processus
- **User** permet de faire du mapping des UID / GID entre l'hôte et ses containers


## 3.4 Machine virtuelle vs Container<a name="vms-vs-containers"></a>

On compare souvent les machines virtuelles aux containers, mais l'approche des containers est beaucoup plus légère que celle des machines virtuelles car ils partagent le noyau là où chaque machine virtuelle utilise son propre système d'exploitation.

On peut donc faire fonctionner plus de containers sur une seule machine hôte que dans le cadre d'un hyperviseur.
![Containers vs Virtual Machines](./images/containers-vs-vms.png)



## 3.5 Architecture Microservices<a name="microservices"></a>

Grâce à Docker on peut facilement envisager de déployer une architecture dont voici les principaux avantages :

On retiendra surtout que **Docker n'introduit pas d'overhead** (temps passé par un système à ne rien faire d'autre que de se gérer), améliorant de ce fait les performances.

- Découpage de l'application en **processus (services) indépendants**
- Chaque microservice est **responsable de sa couche métier**
- Le découpage du **développement** peut se faire **par équipe**, chacune ayant en charge une brique métier
- Par essence les microservices permettent de **mixer différentes technologies** ce qui permet notamment de remplacer un container développé en Python par un container plus performant, développé en GOlang par exemple
- La **scalabilité** des microservices est **horizontale**. Chaque service étant indépendant, il est facile d'ajuster les perfomances de l'infrastructure
- L'ajout de fonctionnalités est simplifiée

![Microservices](./images/microservices.png)


Malgré tout ce type d'infrastructure engendre des inconvénients :
  
- Il nécessite des interfaces bien définies ([**single writter pattern**](https://richygreat.medium.com/single-writer-principle-9e2f78961390), [**disrupter pattern**](https://lmax-exchange.github.io/disruptor/)...)

- Le coût des tests d'intégration peut être élevé

- La complexité est **déplacée au niveau de l'orchestration** de l'application globale


# 4) Et le DevOps dans tout ça ? <a name="devops"></a>

Docker s'inscrit dans une logique devOps dont l'objectif principal est de la confiance au sein des équipes qui mettent en place cette culiture, et ce via :
- La réduction des temps de livraison grâce à l'industrialisation
- Des déploiements réguliers
- La mise en place de tests unitaires, d'intégration etc
- L'amélioration continue
- L'industrialisation des processus :
  - Provisionning
  - Infrastructure As Code (IAC)
  - Intégration Continue / Déploiement continue : CI / CD
  - l'observabilité, le monitoring

![Outils Devops](./images/devops.jpg)


# 5) L'architecture Docker <a name="docker"></a>

## 5.1 Client / Serveur <a name="client-sever"><a>

Docker s'apuie sur un modèle client / serveur. Le binaire écrit en [Go](https://golang.org/) communique avec le daemon **dockerd** via une [api rest](https://docs.docker.com/engine/api/).

Le processus dockerd gère les images, networks, volumes, etc et délègue le management des containers à [containerd](https://containerd.io/), brique centrale de l'architecture qui est produite et suivie par la [Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/).

L'api est exposée via la socket **/var/run/docker.sock** mais peut aussi être configurée pour écouter sur une socket TCP et donc être accessible via un client distant (**ports 2375 et 2376**).
![Docker client / server](./images/client-server.png)

## 5.2 Concepts essentiels <a name="main-concepts"></a>

- Docker facilite l'utilisation des containers Linux en cachant la complexité sous-jacente
- Un container est lancé à partir d'une image
- Une image contient une application et toutes ses dépendances
- Un fichier **Dockerfile** est utilisé pour la création d'une image
- Une image est distribuée via un registry
- Les containers lancés sur un hôte Docker unique ou sur un ensemble d'hôtes peuvent être regroupés sur un outil d'orchestration tel que [Kubernetes](https://kubernetes.io/).
![SChéma Global de Docker](./images/docker-architecture.svg)


# 6) Les containers <a name="containers"></a>

## 6.1 Création <a name="containers-creation"></a>

Créer un container revient à lancer un processus dans un contexte d'isolation. Ce container est créé à partir d'une image contenant un système de fichiers complet, ainsi que toutes les dépendances nécessaires au fonctionnement d'une application. Ce container est disponible via un registry.

Le lancement d'un container s'effectue de la manière suivante :

```shell
docker container run [OPTIONS] image [COMMAND] [ARG]
```

 L'ensemble des commandes est disponible à l'adresse suivante :
[https://docs.docker.com/engine/reference/commandline/run/](https://docs.docker.com/engine/reference/commandline/run/)


## 6.2 Mode intéractif <a name="interative-mode"></a>

Soit l'exemple suivant :

```shell
docker container run -it -i ubuntu bash
```
Le flag **-t** ajout un pseudo [TTY](https://fr.wikipedia.org/wiki/Tty_(Unix)) et **-i** garde le [stdin](https://fr.wikipedia.org/wiki/Flux_standard) ouvert. Le shell est alors accessible depuis un terminal.

## 6.3 Foreground vs Background <a name="foreground-vs-background"></a>

Par défaut un container est lancé en foreground. Toutefois si l'option **-d** est stipulée, il sera alors lancé en background et l'identifiant du container créé sera retourné.

Dans l'exemple suivant, le container lancera un ping sur les dns de Google au démarrage du container
```shell
docker container run -d alpine ping 8.8.8.8
```

```shell
docker container run -d nginx
e1604d2535b5d479adb74356c43a26b9962a459ecdf62304b7be8dbf9ffa91ea
```

## 6.4 Publication de port <a name="port-publication"></a>
Dans l'exemple précédent nous avons lancé une instance de l'image [Nginx](https://nginx.org/) en tâche de fond. Toutefois celui-ci n'est pas acecssible depuis l'extérieur. Pour palier à cette problématique il convient de binder un port.

```shell
docker container run -d -p 8080:80
```

On peut alors y accéder depuis [http://localhost:8080/](http://localhost:8080). Le port 8080 de la machine hôte est donc bindé sur le port 80 du container. On parle d'allocation statique.

Il est toutefois possible d'alloue dynamiquement un port via le flag **-P**
```docker container run -d -P nginx```

Docker utilise alors un port dont le range est compris entre **32768** et **65535**. Il est à noter que si un port est déjà utilisé, le daemon retournera une erreur.


## 6.5 Bind mount <a name="bind-mount"></a>

Le bind de répertoire ou de fichier consiste à monter une source issue de la machine hôte sur un répertoire ou fichier du container à l'aide de l'option **-v** ou **-mount**.

```shell
docker container run -v $PWD/src:/var/www/html httpd
```

```shell
docker run --mount type=bind,src=$PWD/src,dst=/var/www/html httpd
```

Dans les 2 cas il est à noter l'usage de la variable d'environnement $PWD, indispensable au bon fonctionnement de la commande ; Docker ne sachant pas utiliser de chemin relatif dans le cas présent.

Lors du dévelopement d'une application web il est par exemple très pratique de pouvoir mapper directement les sources du projet sur le vhost du serveur http installé dans le container.


Attention toutefois au bind-mount. Il est de bon ton de limiter les permissions en read-only lorsque l'on mappe un volume sur le container. un montage tel que le suivant pourrait avoir des effets désastreux
```shell
docker container run -v /:/host -ti alpine
```

Un autre cas d'usage très pratique du bind consiste à permettre au container d'utiliser la socket de docker afin de pouvoir contrôler le daemon docker depuis le container. C'est par exemple le cas de [Portainer](https://www.portainer.io/), un outil dédié à cet usage.

Lorsque l'on lance un container avec bind mount de la socket il devient alors possible d'utiliser l'api de dockerd

```shell
docker container run -d --name test -ti -v /var/run/docker.sock:/var/run/docker.sock alpine sleep infinity
docker exec -it test apk add curl
docker exec -it test curl --unix-socket /var/run/docker.sock -X POST http://localhost/v1.41/containers/[CONTAINER_ID]/start
```

On pourra également s'attacher au daemon de docker afin d'écouter en temps réel les différents événements.

```shell
curl --unix-socket /var/run/docker.sock http://localhost/events
```

L'ensemble des possibilités offertes par l'api de Docker est accessible à l'adresse suivante : [https://docs.docker.com/engine/api/sdk/examples/
](https://docs.docker.com/engine/api/sdk/examples).


## 6.6 Portainer <a name="portainer">
[Portainer](https://portainer.io) a pour vocation de simplifier la gestion des différents containers en proposant une interface graphique accessible via un navigateur web à l'adresse suivante [http://localhost:9000](http://localhost:9000).

```shell
docker container run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
```

## 6.7 Limiter les ressources <a name="limit-resources"></a>
Par défaut aucune limite n'est appliquée aux containers : CPU, RAM et I/O sont illimités. Les ressources étant partagées via le kernel, il est nécessaire d'imposer des limites afin de ne pas impacter le reste du système.

```shell
# Lance un container dont le seul but est de consommer de la ram et qui sera tué après 32Mb de ram consommés
docker container run --memory 32m estesp/hogit

# Lance un container visant à stresser la machine hôte en consommant 4 cpus à 100% de leur capacité 
docker container run --rm --cpus 4 progrium/stress --cpu 4
```

La liste des limitations applicables à un container est disponible sur [https://docs.docker.com/config/containers/resource_constraints/](https://docs.docker.com/config/containers/resource_constraints/).

## 6.8 Quelques commandes utiles <a name="usefull-cmd"></a>

Spécifier le nom du container

```shell
docker container run -d --name sancho alpine:3.7 sleep 1000
 ```

Supprimer un container automatiquement lorsqu'il est stoppé

```shell
docker container run --rm --name debug alpine:3.7 sleep 1000
```

Redémarrer automatiquement un container

```shell
docker container run --name api --restart=on-failure node
```

## 6.9 Commandes de base <a name="basic-cmd"></a>

```shell
docker container [CMD] [ARGS]
```

| Commande | Description |
| -------- | ----------- |
| run      | Démarre un container  |
| ls       | Liste les containers |
| inspect  | Liste un container |
| logs     | Visualise les logs d'un container |
| exec     | Exécute un processus au sein d'un container |
| stop     | Arrête un container |kk
| rm       | Supprime un container |

Lister les containers actifs

```shell
docker container ls
```

Lister les containers actifs et stoppés
```shell
docker container ls -a
```

Lister les ids des containers actifs et stoppés
```shell
docker container ls -a -
```Veille : Définir un sujet

Inspecter un container
```shell
docker inspect [CONTAINER_ID_OR_NAME]
```

Visualiser les logs d'un container
```shell
docker container logs -f [CONTAINER_ID_OR_NAME]
```

Exécuter une commande au sein d'un container
```shell
docker container exec -it [CONTAINER_ID_OR_NAME] sh
```

Stopper un container
```shell
docker stop [CONTAINER_ID_OR_NAME]
```

Stopper tous les containers
```shell
docker stop $(docker container ls -)
```

Supprimer un container
```shell
docker container rm [CONTAINER_ID_OR_NAME]
```

Supprimer tous les containers
```shell
docker container rm -f $(docker container ls -aq)
```

# 7) Les images <a name="images"></a>

## 7.1 Définition <a name="images-definition"></a>

Une image docker est une sorte de template destiné à être instancié et qui contient une application accompagnée de l'ensemble de ses dépendances logicielles.

Elle se veut portable sur n'importe quel environnement où tourne Docker.

Elle est composée d'un ou plusieurs layers, chacun contenant un système de fichiers et des metadatas.
Les images sont ensuite distribuées via le registry de Docker.


## 7.2 Union Filesystem <a name="aufs"></a>
Une image est un ensemble de couches (layers) accessibles en lecture seule qui composent le système de fichiers final.
Lors du démarrage du container, celui-ci ajoute un layer en lecture-écriture lié au container qui n'affecte pas les couches précédents.

Par défaut les layers sont stockés dans /var/lib/docker

![Aufs FileSystem](./images/aufs.jpg)

![Aufs mechanism](./images/aufs2.jpg)


## 7.3 Dockerfile<a name="dockerfile"></a>

Un Dockerfile est un fichier texte qui contient une série d'instructions pour construire le système de fichiers d'une image.

Le workflow standard consiste à :
- Spécifier l'image de base
- Ajouter les dépendances
- Ajouter du code applicatif
- Définir la commande de démarrage
- Builder l'image à l'aide de docker image build

Voici un exemple de Dockerfile
```shell
#Image de base
FROM node:alpine

# Crée l'arborescence
RUN mkdir /app

# Définition du répertoire de travail
WORKDIR /app

# Copie la liste des dépendances dans le workdir
COPY package.json .

# Installation des dépendances
npm 

# Expose le port 80
EXPOSE 80

# Commande exécutée au démarrage du container
CMD ["npm", "start"]
```

## 7.4 Principales instructions <a name="images-instructions"></a>
| Instruction | Description |
| ----------- | ----------- |
| ADD / COPY  | Copie une ressource à partir de la machine hôte vers le filesystem de l'image |
| ENV         | Définit une variable d'environnement |
| EXPOSE      | Expose un port de l'application |
|`FROM        | Définit l'iamge de base utilisée pour la construction de l'image |
| HEALTHCHECK | Vérifie l'état de santé du container |
| RUN         | Exécute une commande au sein de l'image |
| USER        | Définit l'utilisateur exécutant une instruction au sein du container |
| VOLUME      | Définit un volume la gestion des données |

L'ensemble des commandes est disponible sur la documentation de docker [https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder).

- **ADD / COPY**
Les instructions ADD et COPY ont le même objectif : copier des fichiers et répertoires dans le système de fichiers de l'image. Toutes deux engendre la création d'un nouveau layer. Contrairement à COPY, l'instruction ADD permet de spécifier une url pour la source, tel qu'uun fichier tar.gz. 
- **ENV**
Elle définit les variables d'environnement. Elle pourra être utilisée dans les instructions suivantes du build. Enfin elle sera accessible dans l'environnement des containers créés à partir de cette image.

- **EXPOSE**
Cette instruction informe sur les ports utilisés par l'application, que l'on peut redéfinir via le flag -p au démarrage du container

- **FROM**
L'instruction est la première présente dans un Dockerfile. Elle sert à spécifier l'image à partir de laquelle l'image sera construite

- **HEALTHCHECK**
Souvent oubliée, elle permet de vérifier l'état de santé du container. Son usage fait parti des bests practices, étant donné qu'elle permettra à un orchestrateur de redémarrer un service en mauvais état


- **RUN**
Elle permet d'exécuter une commande dans un nouveau layer. Elle permet par exemple l'installation d'un package.

- **USER**
L'option USER permet de définir l'utilisateur qui exécutera une instruction lors de la création de l'image ou à l'intérieur du container.

- **VOLUME**
L'instruction a pour objectif de définir un répertoire dont les données seront découplées du cycle de vie du container, ce qui signifique qu'en cas de suppression du container, les données seront persistées.


# 8) Stockage <a name="stockage"></a>

## 8.1 Containers et persistance des données <a name="containers-persistance"></a>

Une application containerisée peut persister ses données. Toute modification au sein du container est stockée dans la layer du container créé au démarrage du container, accessible en lecture / écriture.
Lorsque celui-ci est supprimé, ce layer ainsi que tous les fichiers contenus sont automatiquement supprimés. Pour persister les données il est donc indispensable de les gérées en dehors de l'union filesystem

![Aufs](./images/aufs-layers.jpg)

Démarrons un container de test et créons un fichier
```shell
docker container run -it --name test alpine sh
/ # touch /tmp/toto
/ # exit
```

Le fichier est visible depuis la machine hôte
```shell
find /var/lib/docker -name 'test'

/var/lib/docker/btrfs/subvolumes/736077b34945b9a36795ca77545d45cb6952c3321059623451bbbbc0b1e0e106/tmp/toto
```

Supprimons maintenant le container
```shell
docker container rm test
```

Le fichier a été supprimé avec le container
```shell
find /var/lib/docker -name 'toto'
```

## 8.2 Les volumes <a name="volumes"></a>

Les volumes servent à stocker les données à l'extérieur l'union filesystem, c'est à dire à découpler les données du cycle de vie du container. Si un container est supprimé, ses données seront donc toujours présentes.

On utilise par exemple un volume pour le stockage d'une base de données ou encore la persistance de logs en dehors du container.

Un volume peut se définir de plusieurs façons :
- Via l'instruction *VOLUME* dans le *Dockerfile

Pour illustrer cet exemple, lançons une image mongodb...
```shell
docker container run -d --name mongo mongo:5.0.4
```

...et inspectons son contenu
```shell
docker container inspect -f '{{json .Mounts }}' mongo | python -m json.tool

[
  {
     "Type": "volume",
     "Name": "c4df0e1c596d68fcecaaefaa4853ae0e4d04ae4d9df45d2fb6e84188af4b6a43",
     "Source": "/var/lib/docker/volumes/c4df0e1c596d68fcecaaefaa4853ae0e4d04ae4d9df45d2fb6e84188af4b6a43/_data",
     "Destination": "/data/configdb",
     "Driver": "local",
     "Mode": "",
     "RW": true,
     "Propagation": ""
  },
  {
     "Type": "volume",
     "Name": "91aabdd3cb32124bed6c8bc77f6d3b6f944a922c8d78e9117552fd05c0e4bb1a",
     "Source": "/var/lib/docker/volumes/91aabdd3cb32124bed6c8bc77f6d3b6f944a922c8d78e9117552fd05c0e4bb1a/_data",
     "Destination": "/data/db",
     "Driver": "local",
     "Mode": "",
     "RW": true,
     "Propagation": ""
  }
]
```

On constate qu'il existe deux volumes ```/data/configdb``` et ```data/db``` qui ont été définis lors de la création de l'image ; dans le Dockerfile suivant : [https://github.com/docker-library/mongo/blob/1edf40cdc57f393bb5340c298eb6baa98ae94a5f/5.0/Dockerfile](https://github.com/docker-library/mongo/blob/1edf40cdc57f393bb5340c298eb6baa98ae94a5f/5.0/Dockerfile). Les fichiers sont accessibles sur l'hôte dans les répertoires définis par la clé *Source*.

- En utilisant l'option *-v* ou *--mount* lors de la création du container

Pour définir un volume via l'option -v il suffit de lancer
```shell
docker container run -v [CONTAINER_PATH] [IMAGE]
```
Le principe est le même que l'instruction *VOLUME* du Dockerfile : les données contenu dans le path du container seront copié sur la machine hôte. 


- Par l'usage de la commande ```shell docker volume create```
 Le daemon de docker expose au client cli via son api un ensemble de fonctions :

```shell
docker volume --help

Usage:  docker volume COMMAND

Manage volumes

Commands:
  create      Create a volume
  inspect     Display detailed information on one or more volumes
  ls          List volumes
  prune       Remove all unused local volumes
  rm          Remove one or more volumes

Run 'docker volume COMMAND --help' for more information on a command.
```

Pour se faire Docker propose plusieurs drivers dont le *local* qui est utilisé par défaut et pour lequel un répertoire est créé sur la machine hôte ; pour chaque volume.

Illustrons cet usage en créant créant un volume :
```shell
docker volume create --name db-data
```

Il est alors visible par le daemon de docker
```shell
docker volume ls
 ```

```shell
docker volume inspect db-data

[
    {
        "CreatedAt": "2021-11-16T05:53:55+01:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/db-data/_data",
        "Name": "db-data",
        "Options": {},
        "Scope": "local"
    }
]
```

On peut ensuite l'utiliser pour persister les données d'une base de données mongo par exemple
```shell
docker container run -d --name db -v db-data:/data/db mongo:5.0.4
````

Le contenu du répertoire du container est alors visible dans le volume
```shell
ls /var/lib/docker/volumes/db-data/_data
```

# 9) Docker Compose <a name="docker-compose"></a>

## 9.1 Présentation <a name="dc-presentation"></a>

[Docker Compose](https://docs.docker.com/compose/) est un outil permettant de définir et de gérer des applications complexe, comme par exemple une architecture en micro services.

Le principe de base de l'outil consiste à décrire dans un fichier yml l'ensemble des services qui composent notre stack technique et qui sera ensuite lu par un binaire écrit en python *docker-compose*.


## 9.2 Le fichier docker-compose.yml <a name="dc-yml"></a>

Un fichier docker-compose décrit les composants d'une stack technique comme les services, volumes, networks, etc.

Voici un exemple de configuration :

```yml
version: "3.9"
    
services:
  db:
    image: mariadb:latest
    volumes:
      - db_data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    networks:
      - local:
    restart: always
    
  wordpress:
    image: wordpress:latest
    volumes:
      - wordpress_data:/var/www/html
    ports:
      - "8000:80"
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
    networks:
      - local:
    restart: always
    depends_on:
      - db

  phpmyadmin:
    image: phpmyadmin:phpmyadmin
    ports:
      - "8001:80"
    networks:
      - local:
    restart: always
    depends_on:
      - db

volumes:
  db_data:
  wordpress_data:

networks:
  local:
```

- On commence par spécifier la version du fichier, ici *3.9*. Cette version doit être définie en corrélation avec la version du [daemon de Docker](https://docs.docker.com/compose/compose-file/).
- On déclare ensuite un ensemble de services : db, wordpress, phpmyadmin. Chacun de se ces services sont basées sur une image et à la manière d'une exécution docker-cli possèdent des volumes, des variables d'environnement ou encore des ports.
- Enfin on déclare les volumes, networks qui sont utilisés par la stack.


Concrètement, le binaire docker-compose (acessible via ```docker compose``` sur les versions récentes de Docker) effectue les mêmes opérations que l'on pourrait être amené à écrire via docker-cli.
L'ensemble des éléments configurables sont disponibles via la [spécification officielle](https://github.com/compose-spec/compose-spec/blob/master/spec.md) ou encore via la [documentation de Docker](https://docs.docker.com/compose/compose-file/). Les principaux intérêts de l'outil sont de permettre de lancer les applications via ```docker-compose up``` indépendamment du langage, de la plateforme utilisée ou encore de créer des stacks en fonction des environnements d'éxécution.

Malheureusement, la richesse de l'outil, tout comme son évolution constante ne permet pas d'en sortir un résumé efficace et surtout suivi dans le temps. Toutefois, la [documentation officielle](https://docs.docker.com/compose/) couvre parfaitement l'ensemble des possibilités.


## 9.3 Le binaire docker-compose <a name="dc-binary"></a>

Le binaire *docker-compose* permet de gérer le cycle de vie de l'application au format Compose. Son [installation](https://docs.docker.com/compose/install/) est indépendance de Docker mais il se trouve être packagé par défaut avec Docker-desktop sur Mac, Windows.

La commande *docker-compose* utilise le format suivant :
```shell
docker-compose [-f <arg>...] [--profile <name>...] [options] [COMMAND] [ARGS...]

docker-compose -f docker-compose.yml up -d --force-recreate
```
L'instruction -f est optionnelle. Elle sert à spécifier le ou les fichiers utilisés par la stack ; là où le binaire recherche par défaut un fichier *docker-compose.yml*.


Les principales commandes sont les suivantes :

| Commande          | Description |
| ----------------- | ----------- |
| up / down         | Création / Suppression d'une application (services, volumes, réseaux)  |
| start / stop      | Démarrage / Arrêt d'une application  |
| build             | Build des images et des services |
| pull              | Téléchargement des images |
| logs              | Visualisation des logs de l'application |
| scale             | Modification du nombre de containers pour un service |
| ps                | Liste les containers de l'application |

La liste complète des possibilités de docker peut être obtenue à l'aide de ```docker compose --help```
```shell
Define and run multi-container applications with Docker.

Usage:
  docker-compose [-f <arg>...] [--profile <name>...] [options] [COMMAND] [ARGS...]
  docker-compose -h|--help

Options:
  -f, --file FILE             Specify an alternate compose file
                              (default: docker-compose.yml)
  -p, --project-name NAME     Specify an alternate project name
                              (default: directory name)
  --profile NAME              Specify a profile to enable
  --verbose                   Show more output
  --log-level LEVEL           Set log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
  --no-ansi                   Do not print ANSI control characters
  -v, --version               Print version and exit
  -H, --host HOST             Daemon socket to connect to

  --tls                       Use TLS; implied by --tlsverify
  --tlscacert CA_PATH         Trust certs signed only by this CA
  --tlscert CLIENT_CERT_PATH  Path to TLS certificate file
  --tlskey TLS_KEY_PATH       Path to TLS key file
  --tlsverify                 Use TLS and verify the remote
  --skip-hostname-check       Don't check the daemon's hostname against the
                              name specified in the client certificate
  --project-directory PATH    Specify an alternate working directory
                              (default: the path of the Compose file)
  --compatibility             If set, Compose will attempt to convert deploy
                              keys in v3 files to their non-Swarm equivalent

Commands:
  build              Build or rebuild services
  bundle             Generate a Docker bundle from the Compose file
  config             Validate and view the Compose file
  create             Create services
  down               Stop and remove containers, networks, images, and volumes
  events             Receive real time events from containers
  exec               Execute a command in a running container
  help               Get help on a command
  images             List images
  kill               Kill containers
  logs               View output from containers
  pause              Pause services
  port               Print the public port for a port binding
  ps                 List containers
  pull               Pull service images
  push               Push service images
  restart            Restart services
  rm                 Remove stopped containers
  run                Run a one-off command
  scale              Set number of containers for a service
  start              Start services
  stop               Stop services
  top                Display the running processes
  unpause            Unpause services
  up                 Create and start containers
  version            Show the Docker-Compose version information
```


# 10) Network <a name="network"></a>

## 10.1 Container Network Model <a name="network-cnm"></a>
Docker s'appuie sur le **Container Network Model (CNM)** pour la configuration des interfaces réseaux des containers Linux.

![CNM](./images/network.jpg)

Le CNM repose sur 3 composants :
- **Sandbox** : stack réseau d'un container
- **Endpoint** : interface réseau qui relie un container à un réseau
- **Network** : groupe de endpoints qui peuvent communiquer ensemble

Pour implémenter les drivers du CNM, Docker utilise différentes technologies :
- bridges linux
- namespaces réseau
- paire d'interfaces virtuelles (veth pairs)
- iptables

L'ensemble de ces éléments fournissent différentes fonctionnalités :
- Règles de gestion du trafic
- Segmentation réseau
- Service discovery
- Load balancing
- Routing mesh (Swarm, deprecated)

## 10.2 Cli <a name="network-cli"></a>
Comme pour les autres primitives Docker fournit un ensemble de fonctionnalités :

```shell
docker network --help

Usage:  docker network COMMAND

Manage networks

Commands:
  connect     Connect a container to a network
  create      Create a network
  disconnect  Disconnect a container from a network
  inspect     Display detailed information on one or more networks
  ls          List networks
  prune       Remove all unused networks
  rm          Remove one or more networks

Run 'docker network COMMAND --help' for more information on a command.
```

## 10.2 Drivers <a name="network-drivers"></a>
La commande *docker network create* permet de créer un nouveau network en spécifiant le driver à utiliser :

```shell
docker network create --driver DRIVER [OPTIONS] NAME
```
Suivant le driver utilisé, il sera possible d'utiliser certaines fonctionnalités

Par défaut plusieurs drivers sont disponibles :
- host
- none
- bridge
- overlay
- macvlan

Il est possible d'en installer d'autres, compatibles avec le CNM via des plugins comme par exemple [Weave](https://www.weave.works/docs/net/latest/overview/), [Calico](https://www.tigera.io/project-calico/), [Flannel](https://github.com/flannel-io/flannel).


## 10.2 Networks d'un hôte Docker <a name="network-host"></a>
Par défaut Docker crée 3 networks

```shell
docker network ls

NETWORK ID     NAME          DRIVER    SCOPE
f1c0b9915771   bridge        bridge    local
8b4b32a64a0f   host          host      local
6ebccb73a16b   none          null      local
```

- **Bridge**

Créé sur la machine hôte et représenté par le bridge Linux *docker0* il permet à l'ensemble des containers de pouvoir dialoguer. Par défaut, tous les containers démarrés sont visibles via ce bridge.

```shell
ip a show docker0

5: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:4d:d7:a8:ff brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
```

Il est intéressant de noter qu'avec du bridge, la communication est limitée aux containers d'une **même machine**.

![Bridge docker0](./images/network-bridge.png)

Pour pouvoir fonctionner le bridge, crée une paire d'interfaces virtuelles, l'une étant connectée au container et l'autre à la machine hôte.

Pour illustrer listons les interfaces bridges présentes sur la machine hôte
```shell
brctl show

bridge name	bridge id		STP enabled	interfaces
docker0		8000.02424dd7a8ff	no		
```

Démarrons ensuite un container et regardons les interfaces réseaux
```shell
docker container run -ti alpine sh
ip a

20: eth0@if21: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

Regardons à nouveau les interfaces bridges
```shell
brctl show

bridge name	bridge id		STP enabled	interfaces
docker0		8000.02424dd7a8ff	no		vethf8ac169
```

Le container démarré est donc visible sur le bridge docker0.

Le mode bridge est cependant limité : il n'est pas possible de faire de la résolution de noms dans ce mode.

Pour illustrer démarrons un container basé sur l'image nginx

```shell
docker run -d --name=nginx

0a8514c66bf6946f55da967598777d3e7f4055f64f1dff55d5b7fb12cf446b72
```

Récupérons l'ip du container

```shell
ocker inspect -f '{{.NetworkSettings.IPAddress}}' 0a8514

172.17.0.3
```

Démarrons un nouveau container et essayons de pinger le container par son nom, puis son ip
```shell
docker run -it alpine sh

/ # ping -c3 nginx
ping: bad address 'nginx'

ping -c3  172.17.0.3
```

Inspectons maintenant les détails du network bridge docker0

```shell
docker network ls

NETWORK ID     NAME      DRIVER    SCOPE
f1c0b9915771   bridge    bridge    local
8b4b32a64a0f   host      host      locanl
6ebccb73a16b   none      null      local
```

```shell
docker inspect f1c0b9915771

[
    {
        "Name": "bridge",
        "Id": "f1c0b99157713ab21716758a9405929ddee5ce46589090bdc5d118d7b436d43b",
        "Created": "2021-11-27T09:24:28.563137709+01:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "0a8514c66bf6946f55da967598777d3e7f4055f64f1dff55d5b7fb12cf446b72": {
                "Name": "nginx",
                "EndpointID": "f4a26177ab1ecf3d20724f7cc0f646c8b19464b324c93ecd59190cc4ae634ea6",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            },
            "d557cc0af0adedc5c4aec2906f9042e582502e7827d17dd135394f7df6156ac1": {
                "Name": "blissful_swartz",
                "EndpointID": "4af5dbf4ca3f3acb47ec13765206c6d3a58987029fc9f59bf980b2522fbc0b95",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

On constate que les container précédents sont attachés au bridge par défaut étant donné qu'aucun network n'a été spécifié au démarrage des containers. En outre on illustre également ici la connectivité via des paires virtuelles, la machine hôte étant accessible via ```172.17.0.1``` là où par exemple le container nginx a pour ip ```172.17.0.2```.



- **Host**

Il permet d'accéder à la stack réseau de la machine hôte.

Démarrons un container
```shell
docker container run -it --network=host alpine sh
```

Listons les networks depuis le container **et** depuis la machine hôte
```shell
ip link

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp9s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP qlen 1000
    link/ether 04:92:26:d4:b1:d5 brd ff:ff:ff:ff:ff:ff
3: wlp6s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN qlen 1000
    link/ether be:d0:cd:94:cc:03 brd ff:ff:ff:ff:ff:ff
5: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN 
    link/ether 02:42:4d:d7:a8:ff brd ff:ff:ff:ff:ff:ff
15: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN qlen 1000
    link/ether 52:54:00:74:28:1f brd ff:ff:ff:ff:ff:ff
```

On constate que les résultats sont identiques, le container a accès aux mêmes interfaces.

- **None**

Il donne à un container une interface locale et ne lui permet pas de dialoguer avec l'extérieur

Démarrons un container avec le driver none et listons les interfaces réseaux depuis celui-ci

```shell
docker container run -it --network none alpine sh

/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
```

On constate que seul le loopback est initialisé. Il peut être utile pour faire en sorte que le container démarré ne puisse pas être accédé depuis l'extérieur ; dans le cadre d'un container de debug par exemple.

## 10.3 Création d'un network bridge : user defined bridge<a name="network-bridge"></a>

Il est possible de créer un network de type bridge qui pourra être ensuite utilisé pour connecter des containers. Comme pour docker0 une paire d'interface bridge sera créée, dont une paire sera associée au container et dont l'autre sera connectée au namespace network root, c'est à dire à la machine hôte.

L'intérêt principal est de permettre la résolution DNS, ce qui n'est pas possible avec le docker0. C'est ce type de bridge qui sera créé par docker-compose lorsque que l'on définit les différents networks utilisables par les services. Pour aller plus loin la [documentation du driver](https://docs.docker.com/network/bridge/) précise son fonctionnement.

![User Bridge](./images/network-bridge-user.png)

Créons un nouveau nework de type bridge
```shell
docker network create --driver=bridge skynet

44568baae9fa5f2b8a27c3b03919ef1db97f0fcbcc45d3149b954f15089f990b
```

Listons les réseaux dispnoibles

```shell
docker network ls

NETWORK ID     NAME      DRIVER    SCOPE
f1c0b9915771   bridge    bridge    local
8b4b32a64a0f   host      host      local
6ebccb73a16b   none      null      local
44568baae9fa   skynet    bridge    loca
```

Le réseau *skynet* est désormais disponible

Démarrons un container nginx sur ce network

```shell
docker run -d --name=nginx --network=skynet nginx
```

On peut désormais pinger ce container depuis un autre container

```shell
docker run -ti --name=alpine --network=skynet alpine sh

/ # ping -c3 nginx
PING nginx (172.18.0.2): 56 data bytes
64 bytes from 172.18.0.2: seq=0 ttl=64 time=0.094 ms
64 bytes from 172.18.0.2: seq=1 ttl=64 time=0.096 ms
64 bytes from 172.18.0.2: seq=2 ttl=64 time=0.095 ms

--- nginx ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 0.094/0.095/0.096 ms
```


# 11) Gestion des logs <a name="logs"></a>

## 11.1 Persistance <a name="logs-persistance"></a>
Dans le cycle de vie d'une application, il est toujours nécessaire d'accéder aux logs, afin par exemple, de débugguer l'application. Pour se faire, dans le cadre d'un environnement de production **il ne faut pas sauvegarder les logs dans un container**.

Les logs sont stockés dans la layer du container liée à son cycle de vie ; celui créé au démarrage du container et qui est accessible en lecture et écriture. Si le container est supprimé, ils seront donc perdus. Il est recommendé de logger sur la sortie standard (*stdout*) et l'erreur standard (*stderr*) afin de pouvoir les utiliser via un agrégateur de logs et un stockage centralisé.

## 11.2 Drivers <a name="logs-drivers"></a>
Par défault sont installés plusieurs plugins sur la machine hôte

```shell
docker info | grep "Plugin" -A 3

 Plugins:
  buildx: Build with BuildKit (Docker Inc., v0.6.1-docker)
  compose: Docker Compose (Docker Inc., 2.1.1)

--
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
```


Chaque plugin peut être utilisé via le driver qu'il définit.

Par défaut c'est le drive *json-file* qui sert à la gestion des logs. Celui-ci stockant dans un fichier json (```/var/lib/docker/containers/CONTAINER_ID/CONTAINER_ID-json.log```), on comprend pourquoi il est difficilement exploitable dans un environnement de production ; le fichier étant rattaché à la layer de démarrage du container. 


**Principaux drivers de logs**

| Driver     | Destination |
|------------|-------------|
| json-file  | fichier local (driver par défaut) |
| awslogs    | service AWS CloudWatch |
| gcplogs    | service de logging de Google Cloud Platform (GCP) |
| logentries | service [https://logentries.com](https://logentries.com) |
| spunk      | service [https://spunk.com](https://spunk.com) |
| gelf       | gendpoint GELF (Graylog Extended Log Format), ex: Logstash |
| syslog     | daemon syslog tournant sur la machine hôte |
| fluentd    | daemon fluentd tournant sur la machine hôte |
| journald   | daemon journald tournant sur la machine hôte |

Pour utiliser un driver spécifique il existe 2 solutions :

Pour configurer un driver, on peut le faire via :

- le daemon docker

```shell
nvim /etc/docker/daemon.json

{
  "log-driver": "gelf",
  "log-opts": {
    "gelf-address": "udp://192.168.99.100:5000"
  }
}
```

- le service docker via systemd

```shell
nvim /etc/systemd/system/docker.service.d/10-machine.conf

ExecStart=/usr/bin/dockerd -H fd:// --log-driver=gelf --log-opt gelf-address=udp://192./168.99.100:5000
```

Si toutefois il est impossible d'accéder à la configuration du daemon on peut aussi spécifier le driver à utiliser au moment du lancement du container :

- docker-cli

```shell
docker service create --log-driver=gelf --log-opt gelf-address=udp://10.0.0.18:5000 reverse-proxy:1.0
```

- docker-compose

```
services:
  reverse-proxy:
    image: reverse-proxy:1.0
    logging:
      driver: "gelf"
      options:
        gelf-address: "udp://10.0.0.18:5000"
```


# 12) Sécurité <a name="security"></a>

## 12.1 Quelques rappels <a name="security-reminders"></a>
Un container est un processus qui tourne dans un contexte d'isolation particulier grâce aux [namespaces](#namespaces) et [Cgroups](#cgroups) du kernel Linux.

En plus de ces éléments, il est possible de renforcer la sécurité grâce à :

- SELinux :
[Security-Enhanced Linux](https://github.com/SELinuxProject/selinux), abrégé SELinux, est un Linux security module (LSM), qui permet de définir une politique de contrôle d'accès obligatoire aux éléments d'un système issu de Linux.

- AppArmor :
[AppArmor](https://gitlab.com/apparmor/apparmor) permet à l'administrateur système d'associer à chaque programme un profil de sécurité qui restreint ses accès au système d'exploitation. Il complète le traditionnel modèle d'Unix du contrôle d'accès discrétionnaire (DAC, Discretionary access control) en permettant d'utiliser le contrôle d'accès obligatoire (MAC, Mandatory access control).

- Seccomp : 
[Seccomp](https://github.com/seccomp/libseccomp) permet à un processus d'effectuer une transition unidirectionnelle vers un état de sécurité dans lequel il ne peut plus effectuer d'appel système excepté exit(), sigreturn(), read() et write() sur des descripteurs déjà ouverts. Si le processus essaie d'effectuer un autre appel système, le noyau terminera le processus avec le signal SIGKILL ou SIGSYS.

- Capabilities :
La gestion des capabilities est un mécanisme de sécurité du noyau Linux concourant à assurer un confinement d’exécution des applications s’exécutant sur le système en affinant les possibilités d'appliquer le principe du moindre privilège.

## 12.2 Hardening <a name="security-hardening"></a>

Les menaces liées à un container docker sont multiples :

**Compromission de l'hôte par un container**

- Épuisement des ressources :

on peut imaginer qu'un container lance une fork bomb qui amènerait à consommer l'intégralité des ressources de la machine hôte. Pour se prémunir de ce type de problème il convient de limiter les ressources affectées à un container 

- Accès au filesystem de la machine hôte :

il faut faire attention aux volumes qui sont montés. Selon le principe de least privelege (loi de Demeter) il convietn de ne monter que les dossiers nécessaires au fonctionnement du container et autant que possible en read only.

**Compromission d'un container par un autre container**

- utilisation massive des ressources :

la non limitation des ressources d'un container peut engendrer une indisponibilité de CPU, Ram pour les autres containers. Il faut donc faire attention à limiter l'usage du CPU / Ram via les cgroups.

**Corruption de l'image utilisée**


Un container est basé sur une image qui contient un root filesystem ainsi qu'une application et ses dépendances. L'image peut contenir des vulnérabilités qu'il convient d'adresser. À défaut de pouvoir, il est important d'assurer que celles-ci sont connues et maîtrisées et qu'elles ne risquent pas d'impacter l'application.

Le but du hardening est de mettre en place un maximum d'éléments de sécurité afin de minimiser la surface d'attaque du container. Le CIS fournit à ce sujet un ensemble de [bonnes pratiques à respecter](CIS_Docker_CE_Benchmark_v1.1.0.pdf) qui concernant les éléments suivants :
- Configuration de la machine hôte
- Configuration du daemon Docker
- Fichiers de configuration du daemon Docker
- Images et Dockerfile
- Environnement d'exécution d'un container
- Opérations

## 12.3 Docker Security Bench<a name="security-bench"></a>

Il s'agit d'un outil basé sur les recommandations du CIS qui couvre l'ensemble des différents domaines et assure donc que les bonnes pratiques autour du déploiement des containers Docker en production soit respectées.

Le code source du logiciel est disponible sur Github, [https://github.com/docker/docker-bench-security](https://github.com/docker/docker-bench-security).

Celui-ci peut s'executer depuis un container docker.

```shell
docker run -it --net host --pid host --cap-add audit_control \
-e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
-v /var/lib:/var/lib \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /etc:/etc --label docker_bench_security \
docker/docker-bench-security


Unable to find image 'docker/docker-bench-security:latest' locally
latest: Pulling from docker/docker-bench-security
cd784148e348: Pull complete 
48fe0d48816d: Pull complete 
164e5e0f48c5: Pull complete 
378ed37ea5ff: Pull complete 
Digest: sha256:ddbdf4f86af4405da4a8a7b7cc62bb63bfeb75e85bf22d2ece70c204d7cfabb8
Status: Downloaded newer image for docker/docker-bench-security:latest
# ------------------------------------------------------------------------------
# Docker Bench for Security v1.3.4
#
# Docker, Inc. (c) 2015-
#
# Checks for dozens of common best-practices around deploying Docker containers in production.
# Inspired by the CIS Docker Community Edition Benchmark v1.1.0.
# ------------------------------------------------------------------------------

Initializing Mon Nov 29 17:37:59 UTC 2021


[INFO] 1 - Host Configuration
[WARN] 1.1  - Ensure a separate partition for containers has been created
[NOTE] 1.2  - Ensure the container host has been Hardened
[INFO] 1.3  - Ensure Docker is up to date
[INFO]      * Using 20.10.11, verify is it up to date as deemed necessary
[INFO]      * Your operating system vendor may provide support and security maintenance for Docker
[INFO] 1.4  - Ensure only trusted users are allowed to control Docker daemon
[INFO]      * docker:x:975:whax
[WARN] 1.5  - Ensure auditing is configured for the Docker daemon
[WARN] 1.6  - Ensure auditing is configured for Docker files and directories - /var/lib/docker
[WARN] 1.7  - Ensure auditing is configured for Docker files and directories - /etc/docker
[INFO] 1.8  - Ensure auditing is configured for Docker files and directories - docker.service
[INFO]      * File not found
[INFO] 1.9  - Ensure auditing is configured for Docker files and directories - docker.socket
[INFO]      * File not found
[INFO] 1.10  - Ensure auditing is configured for Docker files and directories - /etc/default/docker
[INFO]      * File not found
[INFO] 1.11  - Ensure auditing is configured for Docker files and directories - /etc/docker/daemon.json
[INFO]      * File not found
[INFO] 1.12  - Ensure auditing is configured for Docker files and directories - /usr/bin/docker-containerd
[INFO]      * File not found
[INFO] 1.13  - Ensure auditing is configured for Docker files and directories - /usr/bin/docker-runc
[INFO]      * File not found

...

[INFO] Checks: 105
[INFO] Score: 15
```

Chaque point défini par le CIS benchmark est analysé permettant de définir une note. Il n'y a rien qui définit ce qu'est une bonne ou une mauvaise note. Toutefois l'ensemble des éléments indiqués comme [WARN] ou [INFO] doivent être adressés en corrélation avec les recommendations du CIS.

## 12.4 Linux Capabilities <a name="security-capabilities"></a>

Les capabilities sont des primitives présentes dans le kernel Linux qui permettent de sécouper les droits root en plusieurs catégories et de casser la dichotomie root vs non root en augmentant la granularité des droits d'accès dont voici les principales :

- **CAP_CHOWN** : permet la modification des uid / gid
- **CAP_SYS_ADMIN** : permet des opérations administratives côté système
- **CAP_NET_ADMIN** : permet des opérations administratives côté network
- **CAP_NET_BIND_SERVICE** : permet d'affecter un port privilégié (< 1024) à un processus

Elles peuvent être ajoutées ou supprimées au lancement d'un container.

Lançons un container et essayons d'en changer le hostname.

```shell
docker run -it alpine sh

/ # hostname foo
hostname: sethostname: Operation not permitted
```

Réeffectuons la même opération en ajoutant une capabilité
```shell
docker run -it --cap-add=SYS_ADMIN alpine sh

/ # hostname foo
/ # hostname
foo
```

Le hostname du container a bien été changé, la capabilité *SYS_ADMIN* ayant rendu l'opération possible.

On peut également illustrer la possibilité de supprimer une capabilité *NET_RAW* dès lors que l'on souhaite effectuer un ping de la manière suivante :

```shell
docker run alpine ping -c3 8.8.8.8


PING 8.8.8.8 (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: seq=0 ttl=113 time=12.579 ms
64 bytes from 8.8.8.8: seq=1 ttl=113 time=12.264 ms
64 bytes from 8.8.8.8: seq=2 ttl=113 time=12.172 ms

--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 12.172/12.338/12.579 ms
```

```shell
docker run --cap-drop=NET_RAW alpine ping 8.8.8.8

PING 8.8.8.8 (8.8.8.8): 56 data bytes
ping: permission denied (are you root?)
```

## 12.5 Linux Security Module <a name="security-module"></a>

Les modules LSM (AppArmor / SELinux) implémentent le MAC (Mandatory Access Control) qui s'implémente au dessus du DAC (Discretionary Access Control) et renforcent la sécurité en ajoutant des droits d'accès supplémentaires.

**AppArmor**

Ce module ajoute des droits basés sur le chemin d'accès aux ressources. Dans ce contexte chaque application a son propre profil de sécurité pour limiter ses droits et pour spécifier les dossiers, fichiers ou appareils auxquels elle peut accéder. Il est par défaut présent sur Ubuntu ou encore OpenSuse

Supposons un container avec un profil AppArmor installé :
```shell
docker run -ti alpine sh
/ # cat /proc/kcore
cat: can't open '/proc/kcore': Permission denied
```

Relançons ce même container en désactivant le profil de sécurité :
```shell
docker run -ti --security-opt apparmor:unconfined alpine sh
/# cat /proc/kcore
/# 
```
Cette fois-ci il n'y a pas d'erreur.

**SELinux**

Ce module de sécurité s'applique sur le système entier, contrairement à AppArmor en définissant des attributs supplémentaires sur le système de fichiers complet.

Le principe repose sur une combinaison d'objets (fichiers, répertoires, devices, interfaces réseaux etc) auxquels peuvent accéder des sujets (utilisateurs, applications, processus).

Son créateur, Daniel J Walsh, propose un [article](https://opensource.com/business/13/11/selinux-policy-guide) expliquant en détails son fonctionnement. Il propose d'ailleurs un [livre à colorier](./selinux-coloring-book.pdf) afin de se familiariser avec le sujet.

Supposons que SELinux soit installé sur la machine hôte et listons les binaires docker

```shell
ls -Z /usr/bin/docker*

-rwxr-xr-x. root root system_u:object_r:docker_exec_t:s0 /usr/bin/docker
-rwxr-xr-x. root root system_u:object_r:docker_exec_t:s0 /usr/bin/dockerd
-rwxr-xr-x. root root system_u:object_r:docker_exec_t:s0 /usr/bin/docker-compose
```

L'argument Z permet de lister les attributs rajoutés par le module, le contexte, nommé ici *docker_exec_t*.

Supposons qu'un serveur Nginx soit installé sur la machine hôte

```shell
ls -Z /usr/share/nginx/html/index.html

-rw-r--r--. root root system_u:object_r:httpd_sys_content_t:s0 index.html
```

Le contexte de sécurité est ici représenté par *httpd_sys_content_t*.

Démarrons un container qui mappe l'intégralité du système sur le container et essayons de modifier l'index de nginx :

```shell
docker run -ti -v /:/host alpine sh

echo "test" > /host/usr/share/nginx/html/index.html

sh: can't create /host/usr/share/nginx/html/index.html: Permission denied
```

SELinux joue bien son rôle et désactive l'accès au fichier en écriture depuis le container.

Pour y remédier il est possible de désactiver le profil :

```shell
docker run -ti -v /:/host --security-opt label:disable alpine sh

/ # echo "test" > /usr/share/nginx/html/index.html
/ #
```

## 12.6 Secure Computing Mode <a name="security-scm"></a>

Seccomp opère au niveau des appels système (+ de 300 pour un linux x86-64) : mount, mkdir, ptrace, reboot, etc. Dans ce contexte le profil par défaut pour docker désactiver 44 de ces appels.

Il est toutefois possible de créer un profil custom, fourni au lancement d'un container. Toutefois il est difficile de déterminer les appels systèmes utilisés par un processus. On pourra utiliser *strace* pour réaliser cette action mais cela nécessitera malgré tout une bonne connaissance du kernel Linux.

Pour pouvoir l'utiliser il est nécessaire que Seccomp soit installé sur la machine hôte.

Il est à noter que certains des appels systèmes sont déjà filtrés au travers des capabilities.

La [documentation de Docker](https://docs.docker.com/engine/security/seccomp/) explique parfaitement l'usage de seccomp. Dans la majorité des cas le profil par défaut fournir par l'installation de seccomp suffira et sera automatiquement appliqué par Docker.

## 12.7 Scanning de CVE <a name="security-cve"></a>

Comme une image contient de nombreuses librairies et fichiers binaires du système de base (Alpine par exemple) en plus du code applicatif, il est important de scanner celle-ci afin de déterminer si elle contient des CVE (Common Vulnerabilities and Exposures).

Il existe de nombreux outils comme :

- [Anchore-Engine](https://github.com/anchore/anchore-engine)
- [Clair](https://github.com/quay/clair) fournit par CoreOS
- [Docker Security Scanning](https://docs.docker.com/engine/scan/), payant au delà de 10 scans / mois.
- [Trivy](https://github.com/aquasecurity/trivy)

Ces solutions assurent soit un scan des fichiers, soit un scan des binaires à la recherche de signatures connues. Elles sont intégrables dans un pipeline d'intégration continue.

Le nombre de CVE existantes étant nombreuses, il est de bon ton de privilégier les images basées sur Alpine, celles-ci ayant une faible surface d'attaque.

## 12.8 Content Trust <a name="security-content-trust"></a>

[Content Trust](https://docs.docker.com/engine/security/trust/) est un mécanisme qui permet de s'assurer de l'intégrité d'une image ainsi que de sa provenance. Il utilise [Notary](https://github.com/notaryproject/notary) un projet open source de Docker qui implémente The Update Framework (TUF) qui permet d'assurer la sécurité des systèmes lors des mises à jour logicielles, notamment en assurant de la chronologie, en vérifiant que les CVE impactants les différents mises à jours.

Content Trust permet à un développeur de signer l'image lors d'un push et à l'utilisateur de vérifier l'intégralité lors du pull. Ainsi, dès lors que la variable d'environnement *DOCKER_CONTENT_TRUST* est à *1*, alors il ne sera plus possible de puller d'images non signées.

Cette signature intervient au moment du tag de l'image.

![Content Trust](./images/trust.png)

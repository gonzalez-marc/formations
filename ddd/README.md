## À propos du Domain Driven Design

Le D.D.D. fait référence aux concepts clés, techniques et patterns décrits dans le livre d'Eric Evans :

> **_Domain Driven Design : Tackling Complexity in the Heart of Software_**.

L'enjeu du DDD est de créer un langage commun (Ubiquitous Language) entre les membres d'une équipe projet (technique, fonctionnel, commercial, métier, etc) afin de faciliter la communication et la compréhension.

Souvent, quand on parle de DDD, on parle de programmation orientée objet faite correctement alors que le DDD est surtout une méthode destinée à affronter la complexité, permettant de découvrir la **_*big picture*_**.

Il encourage la pratique du TDD (Test Driven Development : TDD), l'utilisation de patterns et le refactoring permanent du code

## Définitions : domaine, sous-domaine, bounded context

En DDD un **domaine** représente un problème business : e-commerce, payroll, etc

Un domaine peut être divisé en **sous domaines**. Chaque sous domaine se focalise sur un problème particulier (Ex : panier, paiement, catalogue produits, customer...)

Chaque sous domaine doit avoir ses responsabilités explicitées afin de déterminer quelles sont ses limites, quelles sont ses fonctionnalités. Le fait de définir des domaines permet de faire une et une seule chose mais de le faire bien. Cette frontière est appellée le **bounded context** du sous domaine.

Le bounded context définit :

- Combien de modèles sont nécessaires pour définir le sous domaine ?
- Quelles sont les propriétés de chaque modèle ?
- Quelles sont les fonctionnalités attendues dans le sous domaine ?

Exemple :

> Le panier d'un site e-commerce est un sous domaine qui a besoin des modèles suivants : Panier, Produit, Client. Il contient les fonctionnalités pour faire du CRUD dans le panier.

Notes : les modèles Produit et Client du Panier ne sont pas nécessairement les mêmes que ceux du domaine Catalogue Produits ou encore du sous domaine Client. Ils contiennent seulement les informations nécessaires pour afficher le panier.

## Quel processus adopter ?

![process](./process.jpeg)

## 1 - S'aligner et découvrir

### En définissant un ubiquitous Language

L'usage d'un langage précis est essentiel dans la compréhension et la communication permettant de découvrir les concepts métier.
En DDD, il ne s'agit pas seulement de définir les noms et verbes métier mais surtout de parler des concepts.

L'idée est de comprendre la valeur de l'intention, tout en sachant que pour une intention donnée, il y a plusieurs implémentations possibles.

Tout le monde doit utiliser le même langage partout, tout le temps, afin de que tous soient en accord avec le(s) expert(s) métier(s).

Il convient de mettre de côté l'implémentation dans un premier temps afin de se focaliser sur les éléments métiers implicites, qui, eux, doivent être explicités afin notamment de découvrir les intentions du modèle principal.

Considérons l'exemple suivant :

> Lorsqu'une personne s'inscrit à un cours de découverte du café, et que le cours est complet, la personne se retrouve "en attente". Si une place se libère alors les coordonnées de la personne sont envoyées au système de paiement via un bus de données afin que la demande soit traitée.

| La personne a un statut          | Le statut semble être un flag ou un champ. Peut être que l'expert du domaine est familier avec cette notion dans un autre système, avec par exemple un tableur et qu'il suggère cette implémentation |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Envoyé via notre bus de message  | Il s'agit d'une implémentation technique. Le fait que l'information soit envoyée via un bus de message ne doit pas avoir d'impact sur le domaine                                                      |
| Afin que la demande soit traitée | C'est ambigu. Qu'entend-t-on par "soit traité" ?                                                                                                                                                      |
| Système de paiement              | Il s'agit là encore d'une implémentation. C'est important qu'il y ait cette notion de paiement mais ça n'a pas d'intérêt à ce stade du projet                                                         |

En creusant un peu plus le sujet on se rend compte que ce n'est pas la personne qui a un statut mais la réservation. On passe donc d'une réservation en attente à une réservation validée. Les réservations en attente sont gérées via une liste d'attente.

### Représenter le(s) modèle(s)

Le Domain Driven Design consistant à designer et créer des modèles expressifs, il en ressort une modélisation commune de l'expression du besoin. Il vise à créer des modèles qui sont compréhensibles par tout le monde, et pas uniquement par les développeurs.

**En utilisant le langage**

Une personne qui envisage de suivre une formation recherche des cours en fonction d'un thème, du coût ansi que d'un calendrier de disponibilités.
Quand un cours est réservé, une inscription à celui-ci est générée, que la personne peut annuler ou accepter à une date ultérieure.

**En modélisation les flux métier**

<center>

![bpmn](./bpmn.png)

</center>

**En utilisant du code**

```php
class Person {
  public function bookCourse(Course $course) {
  }
}

abstract class Registration {
  public function accept(): void;
  public function cancel(): void;
}

class ReservedRegistration extends Registration {
}

class AcceptedRegistration extends Registration {
}

interface CourseRepository {
  public function getAll(): array
}
```

### Réfactorer le langage

Quand on utilise un langage commun pour construire un modèle, lors de la réfactorisation du code, il faut également réfactorer le langage pour incorporer les nouveaux termes. Il faut s'assurer que les concepts représentés par le terme sont définis et acceptés en concordance avec les experts métiers.

L'exemple précédent pourrait devenir le suivant :

> Lorsqu'une personne s'inscrit à un cours de découverte du café, une réservation "réservée" est générée. S'il y a de la place et que le paiement a été accepté alors la réservation passe en statut "acceptée". S'il n'y a pas de place disponible, alors la réservation passe en "en attente de place disponible". La liste des réservations est gérée selon le principe de FIFO (first in, first out)

## 2 - Décomposer et connecter

## Bounded contexts

Pour chaque modèle, il faut explicitement définir le contexte dans lequel il se situe. Il n'y a aucune règle concernant sa création mais il est important que tout le monde comprenne les frontières des autres contextes.

Les équipes qui n'ont pas une bonne compréhension du contexte d'un système donné risquent de compromettre les modèles lors de leur intégration dans les différentes contextes.

Les contextes sont notamment créés à partir de :

- la manière dont les structures sont organisées
- la structure et l'architecture du code de base
- l'usage au sein d'une partie du domaine

Dans une optique de consistance et d'unité au sein d'un contexte, il ne faut pas se laisser distraire par la manière dont le modèle est utilisé à l'extérieur du contexte. Les autres contextes peuvent avoir les mêmes modèles mais ne représentant pas tout à fait les mêmes notions métier. Il est courant pour un autre contexte d'utiliser un langage différent de l'ubiquitous language défini dans le première contexte.

## Context Maps

<center>

![contexts](./contexts.jpg)

</center>

Définir le mapping des contextes revient à designer les points de contacts et les traductions d'un ou plusieurs modèles au sein des contextes

### Quelques patterns

Il existe différents patterns permettant de faire du mapping de contextes :

**Shared Kernel**

Il s'agit d'un bounded context qui est un sous domaine du domaine que différentes équipes acceptent de partager. Il nécessite une bonne communication entre les équipes qui doivent être suffisamment matures pour l'implémenter

<center>

![shared kernel](./shared-kernel.png)

</center>

**Customer/Supplier development teams**

Quand un bounded context nourrit d'autres bounded context, alors le bounded context héritant est dépendant du parent. En sachant qui est le contexte parent et qui est l'enfant, les équipes responsables peuvent joindre leurs efforts en partageant les tests d'acceptation pour les interfaces par exemple.

Cela permet à l'équipe consommatrice du contexte principal d'être dans la confidence des développements sans pour autant avoir peur d'une incompatibilité

<center>

![customer](./customer.jpg)

</center>

**Conformist**

Quand l'équipe qui travaille sur le downstream context n'a pas d'influence ou l'opportunité de collaborer avec l'équipe de l'upstream context, alors elle n'a d'autre choix que de se conformer à celui-ci, qui devient le "dictateur" (éclairé ?).

En se conformant de la sorte, l'équipe s'évite de la souffrance, et réduit la complexité engendrée par la volonté de remplacer une interface qui ne l'est pas.

Le principal problème de cette approche réside dans la potentielle mauvaise qualité du modèle présent dans l'upstream context. S'il est bon, celui du downstream sera bon, s'il est mauvais il le sera également.

<center>

![conformist](./conformist.png)

</center>

**Anti-corruption layer**

Quand des contextes existent dans différents systèmes et tentent d'établir une relation, il en résulte des "saignements" d'un modèle du premier contexte vers le second. L'intention des modèles risque d'être perdue dans la combinaison des deux modèles, des deux contextes. Dans ce cas, il est préférable de garder les 2 contextes à part et introduire une couche d'isolation entre les 2 afin de gérer les transactions dans les 2 directions. On parle alors d'anti corruption layer, qui permet aux clients de travailler dans leurs propres termes sur leurs propres modèles.

On utilise ce layer quand on échange avec des systèmes legacy ou des codes bases qui sont destinées à disparaître

<center>

![acl](./acl.png)

</center>

**Separate Ways**

Rien n'oblige à avoir une relation fonctionnelle entre les différents domaines. Par conséquent ce pattern permet de réduire la complexité en isolant complètement les bounded contextes. Les développeurs (et les stakeholders) peuvent se focaliser sur la recherche de solutions sur des scopes limités

<center>

![acl](./separate.png)

</center>

## 3 - Modéliser les domaines

Au travers de la définition des bounded contexts, on s'attache à rendre les modèles expressifs, à révéler leurs intentions plus que leurs implémentations. Il est ensuite nécessaire de modéliser les domaines de manière flexibles, faciles à réfactorer.

### La structure

**Entités**

Les entités sont des classes dont les instances sont identifiables et qui gardent la même identité tout au long de leur cycle de vie. Il peut y avoir des changements sur leurs propriétés mais jamais leur identité ne change.

**Value objects**

Les value objects se veulent léger, immutables et sans identité propre. Alors que leurs valeurs sont importantes, elles ne sont pas juste de simples DTO. Ce sont des objets adaptés à des calculs complexes, à la déportation de logiques complexes en provenance des entités.

Ils sont un bon moyen d'introduire du typage, leurs sides effects étant nuls, tout en permettant d'introduire de la programmation fonctionnelle.

**Cardinalités des associations**

La bi-directionnalité des associations ajoute de la complexité. Dans la mesure du possible, on essaiera de limiter la navigation au travers des objets à un seul sens, en introduisant la notion de direction. Par exemple, en supposant un contexte dans lequel un utilisateur participe à un projet, un projet a donc X utilisateurs. On demandera rarement à un utilisateur les X projets sur lesquels il travaille mais on aura plutôt tendance à lister tous les utilisateurs d'un projet.

Si vraiment on a besoin de récupérer l'ensemble des projets d'un utilisateur, le pattern repository nous permettra d'implémenter la fonctionnalité

**Services**

Parfois il est impossible d'affecter un comportement à une seule et unique classe, qu'il s'agisse d'une entité ou d'un value object. On créera donc un service, en charge d'encapsuler le comportement faisant appel à plusieurs objets

**Aggrégats**

Au fur et à mesure de l'ajout de modèles, le scope devient de plus en plus large et complexe. Il devient difficile de prendre des décisions techniques. Pour palier à cela, on regroupe les modèles au sein d'aggrégats, dont les limites sont bien définies et déconnectées du reste du scope. Chaque aggrégat a une entité qui agit comme "root" ; comme point d'entrée du métier.

Chaque aggrégat créé doit avoir du sens au sein du domaine. Pour pouvoir vérifier son impact, on peut utiliser le test de suppression. Si l'entité root de l'aggrégat est supprimée, il faut checker quels autres objets sont supprimés dans l'aggrégat.

Quelques règles simples pour gérer l'aggrégat :

- La racine a une identité globale et les autres éléments une identité locale
- La racine s'assure que tous les invariants (règles métiers qui restent cohérentes) sont satisfaits.
- Les entités en dehors de l'aggrégat ne font référence qu'à l'entité racine de l'aggrégat
- La suppression supprime tout dans l'aggrégat
- Quand un objet change les invariants restent respectés

## Gérer les cycles de vie

**Factories**

Les factories peuvent gérer le cycle de vie de certains aggrégats. C'est le principe du factory ou builder pattern. Il faut faire attention aux règles de l'aggrégat, particulièrement les invariants.
Pour autant les factories ne sont pas essentielles. Comme tout pattern, elles doivent être utilisées à bon escient.

**Repositories**

Tandis que les factories gèrent le début du cycle de vie, les repositories gèrent le milieu et la fin du cycle de vie. Les repositories doivent déléguer la persistance de l'information à des objets dédiés. Ils peuvent consommer des entités, ou des aggrégats et doivent donc en respecter les règles.

## Gérer les comportements

**Specification Pattern**

L'usage du specification pattern est utile quand il s'agit de modéliser des règles, de la validation ou du filtrage selon certains critères. Il permet de tester qu'un objet satisfait un ensemble des règles définies.

Considérons la classe suivante

```Php
class Project {
  public isOverdue(): bool {}
  public isUnderBugdet(): bool {}
}
```

La spécification pour "overdue" et "underBudget" est découplée du reste du projet ; la responsabilité est confiée à d'autres classes

```Php
public interface ProjectSpecification {
  public isSatisfiedBy(Project $project): bool;
}

class ProjectIsOverdueSpeficiation implements ProjectSpecification {
  public function isSatisfiedBy(Project $project): bool {
  }
}
```

Cela permet de rendre le code client plus lisible et plus flexible également

```Php
if((new ProjectIsOverdueSpeficiation)->isSatifiedBy($myAwesomeProject)) {
}

```

**Strategy pattern**

Le Strategy pattern, appelé également Policy Pattern, est utilisé pour permettre de switcher d'algorithme.

Considérons l'exemple suivant, qui détermine le succès d'un projet en se basant sur 2 calculs :

- a) Le projest est un arrivé à son terme s'il est fini dans les temps
- b) Le projet est ok, s'il ne dépasse pas le budget

```Php
class Project {
  public function isSuccessFullByTime();
  public function isSuccessFullByBudget();
}
```

En appliquant ce pattern on peut encapsuler les calculs dans des classes dédiées

```Php
interface ProjectSuccessPolicy {
  public function isSucessFull(Project $project): bool
}

class SuccessByTime implement ProjectSuccessPolicy {
  public function isSucessFull(Project $project): bool {
  }
}

class SuccessByBudget implement ProjectSuccessPolicy {
  public function isSucessFull(Project $project): bool {
  }
}
```

On réfactore la première classe de la manière suivante

```Php
class Project {
  public function isSuccessFull(ProjectsuccessPolicy $policy) {
    return $policy->isSuccessFull($this);
  }
}
```

**Composite pattern**

L'idée est de faire en sorte que le code client n'ait à composer qu'avec les resprésentations abstractes des éléments composites.

Considérons l'exemple suivant :

```Php
class Project {
  public function listMilestones(): array;
  public function listTasks(): array;
  public function listSubProjects(): array;
}
```

Un sous projet est un projet avec des milestones et des tâches. Une milestone est une tâche due à une date donnée mais sans durée.
Appliquer un pattern composite permet d'introduire un nouveau type d'activité avec différentes implémentations.

```php
interface Activity {
  public function due(): Datetime;
}

class Subproject implements Activity {
  private list(): array {
  }

  public function due(): Datetime {
  }
}

class Milestones implements Activity {
  public function due(): Datetime {
  }

  public function duration(): int {

  }
}
```

On peut alors simplifier de la manière suivante

```Php
class Project {
  public function add(Activity $activity);
  public function list(): array;
}
```

## 4 - Architecture des domaines

Lorsque le focus du design des modèles se concentre sur la création de modèles riches en comportements, alors l'architecture participe fortement à découpler le modèle de l'infrastructure. C'est ici qu'intervient l'architecture hexagonale.

L'architecture des domaines fera l'objet d'ateliers dédiés.
